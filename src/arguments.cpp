/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "arguments.hpp"
#include <charconv> // std::from_chars
#include <system_error> // std::make_error_condition
#include <cstring> // std::strlen, std::strcmp
#include <unistd.h> // getopt

struct arguments arguments {};

namespace {
void print_license() {
    std::cout << R"(License of Tents-Solver: GPLv3+
Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


License of library CaDiCaL: MIT License
Copyright (c) 2016-2020 Armin Biere, Johannes Kepler University Linz, Austria
Copyright (c) 2020 Mathias Fleury, Johannes Kepler University Linz, Austria

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.)" << std::endl;
}

void print_help(std::ostream& os, char const*const program) {
    os << "Tents-Solver: Solver for the tents puzzle for the lecture SAT Solving at LMU Munich in the winter term 2020/2021\n"
          "For authors and license see " << program << " -l\n\n";
    os << "Usage: " << program << " [options]\n\n";
    os << R"(Options:
-h         Display this help text.
-l         Diplay the license of this program including the authors.
-v         Use a more verbose output.
-p <path>  Specify the path from where to load the puzzle.
-o <path>  Write the solution to the file at the specified path.
-u         Use the unoptimised version: deactivate the major performance enhancements.
-z         Measure the time used for CNF generation and solving. Output a csv line.
-c <path>  Write the CNF as DIMACS CNF to the file at the specified path.
-r <path>  Read the output of a SAT solver to decode and validate the solution.
-t <path>  Test/validate the solution at the specified path.
-g <path>  Generate a new puzzle. The newly created puzzle is written to the specified path.
-n <N>     Specify the number of rows (default 10) for the generated puzzle.
-m <M>     Specify the number of columns (default 10) for the generated puzzle.
-d <D>     Use difficulty D (from 1 to 10, default 5) for the generated puzzle.
-s <seed>  Use the following seed (unsigned 32-bit integer) to generate the puzzle.

To read from standard input or write to standard output substitute <path> by '-' (the dash character at ASCII x2D).)";
    os << std::endl;
}
}

/* parse command line arguments to struct arguments
 *
 * return value:
 *   -1 exit with EXIT_FAILURE (error occured)
 *   0 start the solver
 *   1 exit with EXIT_SUCCESS
 */
int parse_arguments(int argc, char* argv[]) {
    bool generate {}; // We want to generate a puzzle

    for (int opt; (opt = getopt(argc, argv, "hlvp:o:uzc:r:t:g:n:m:d:s:")) != -1;)
        switch (opt) {
        case 'h': // print help
            print_help(std::cout, argv[0]);
            return 1;
        case 'l': // print license
            print_license();
            return 1;
        case 'v': // verbose
            arguments.verbose = true;
            break;
        case 'p': // puzzle input file
            arguments.input = true;
            // read from stdin or file
            arguments.input_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'o': // output the solution
            arguments.output = true;
            // write to stdout or file
            arguments.output_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'u': // unoptimised
            arguments.unoptimised = true;
            break;
        case 'z': // measure time
            arguments.time = true;
            break;
        case 'c':  // generate cnf for external solver
            arguments.cnf_output = true;
            // write to stdout or file
            arguments.cnf_output_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'r': // read solution
            arguments.read_solver_output = true;
            // read from stdin or file
            arguments.read_solver_output_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 't': // test/validate
            arguments.validate = true;
            // write to stdout or file
            arguments.validate_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            break;
        case 'g':
            arguments.generate = true;
            // write to stdout or file
            arguments.generate_file = std::strcmp(optarg, "-") == 0 ? nullptr : optarg;
            generate = true;
            break;
        case 'n': { // dimension n
            auto [p,ec] = std::from_chars(optarg, optarg+std::strlen(optarg), arguments.size_n);

            if (ec != std::errc{}) {
                std::cerr << "Error reading dimension n: "
                          << std::make_error_condition(ec).message() << '\n';
                return -1;
            } else if (arguments.size_n == 0) {
                std::cerr << "Error: The number of rows must be greater than 0.\n";
                return -1;
            }

            generate = true;
          } break;
        case 'm': { // dimension m
            auto [p,ec] = std::from_chars(optarg, optarg+std::strlen(optarg), arguments.size_m);

            if (ec != std::errc{}) {
                std::cerr << "Error reading dimension m: "
                          << std::make_error_condition(ec).message() << '\n';
                return -1;
            } else if (arguments.size_m == 0) {
                std::cerr << "Error: The number of columns must be greater than 0.\n";
                return -1;
            }

            generate = true;
          } break;
        case 'd': { // difficulty
            auto [p,ec] = std::from_chars(optarg, optarg+std::strlen(optarg), arguments.difficulty);

            if (ec != std::errc{}) {
                std::cerr << "Error reading difficulty: "
                          << std::make_error_condition(ec).message() << '\n';
                return -1;
            } else if (arguments.difficulty < 1 || arguments.difficulty > 10) {
                std::cerr << "Error: The difficulty must be between 1 and 10.\n";
                return -1;
            }

            generate = true;
          } break;
        case 's': { // seed for the newly generated puzzles
            auto [p,ec] = std::from_chars(optarg, optarg+std::strlen(optarg), arguments.seed);

            if (ec != std::errc{}) {
                std::cerr << "Error reading seed: "
                          << std::make_error_condition(ec).message() << '\n';
                return -1;
            }

            arguments.use_seed = true;
            generate = true;
          } break;
        default: // '?'
            std::cerr << '\n';
            print_help(std::cerr, argv[0]);
            return -1;
        }


    // We have output arguments
    bool const output = arguments.output // output to file
                        // output CNF (only counts as output if we do not read solver output)
                        || (arguments.cnf_output && !arguments.read_solver_output)
                        || arguments.time; // output measured time

    // We want to input and output
    bool const input_output = (!arguments.validate && !generate) // neither validate nor generate
        || arguments.input // input puzzle
        || arguments.read_solver_output // read from solver output 
        || output; // output arguments

    // We need an input puzzle
    if (input_output && !arguments.input) {
        arguments.input = true;
        
        if (optind < argc) { // Use argv[optind] as input
            std::cerr << "Warning: Using " << argv[optind] << " as input puzzle! "
                         "Please use option -p to specify the input puzzle.\n";
            // read from stdin or file
            arguments.input_file = std::strcmp(argv[optind], "-") == 0 ? nullptr : argv[optind];
            ++optind;
        } else { // Read from stdin
            std::cerr << "Warning: No input puzzle specified! Reading from standard input.\n";
            arguments.input_file = nullptr;
        }
    }

    // We need an solution output
    if (input_output && !output) {
        arguments.output = true;
        
        if (optind < argc) { // Use argv[optind] as output
            std::cerr << "Warning: Using " << argv[optind] << " as solution output! "
                         "Please use option -o to specify the solution output.\n";
            // write to stdout or file
            arguments.output_file = std::strcmp(argv[optind], "-") == 0 ? nullptr : argv[optind];
            ++optind;
        } else { // Write to stdout
            std::cerr << "Warning: No solution output specified! Writing to standard output.\n";
            arguments.output_file = nullptr;
        }
    }

    // We need an output for the generated puzzle
    if (generate && !arguments.generate) {
        arguments.generate = true;

        if (optind < argc) { // Use argv[optind] as output
            std::cerr << "Warning: Using " << argv[optind] << " as output for the generated puzzle! "
                         "Please use option -g to specify the output for the generated puzzle.\n";
            // write to stdout or file
            arguments.generate_file = std::strcmp(argv[optind], "-") == 0 ? nullptr : argv[optind];
            ++optind;
        } else { // Write to stdout
            std::cerr << "Warning: No output for the generated puzzle specified! "
                         "Writing to standard output.\n";
            arguments.generate_file = nullptr;
        }
    }

    // Unnecessary arguments
    if (optind < argc) {
        std::cerr << "Warning: Unnecessary arguments, ignoring";

        for (char** arg = argv+optind; arg < argv+argc; ++arg)
            std::cerr << ' ' << *arg;

        std::cerr << '\n';
    }

    return 0;
}
