/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timer.hpp"
#include <iostream> // std::cout

std::clock_t gen_start = -1;
std::clock_t gen_end = -1;
std::clock_t solver_start = -1;
std::clock_t solver_end = -1;

void print_time_csv(std::size_t num_clauses, std::size_t num_vars, char const* input_file) {
    std::clock_t gen_time = -1;
    std::clock_t solver_time = -1;

    if(gen_start != -1 && gen_end != -1)
        gen_time = gen_end - gen_start;

    if(solver_start != -1 && solver_end != -1)
        solver_time = solver_end - solver_start;

    // gen_time, solver_time, input_file, num_clauses, num_variables, CLOCKS_PER_SEC
    
    if (gen_time > 0)
        std::cout << gen_time;
    std::cout << ", ";

    if (solver_time > 0)
        std::cout << solver_time;
    std::cout << ", ";

    if (input_file)
        std::cout << '"' << input_file << '"';
    std::cout << ", ";

    std::cout << num_clauses << ", " << num_vars << ", " << CLOCKS_PER_SEC << std::endl;
}
