/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TENTS_HPP
#define TENTS_HPP

#include <vector>
#include <array>
#include <optional>
#include <algorithm> // std::find
#include <numeric> // std::accumulate
#include <iosfwd> // std::istream, std::ostream
#include <cstdint> // std::uint_least8_t, std::uint_32t
#include <cstddef> // std::size_t
#include <cassert>

// Class for Tents board
class Tents {
public:
    // Content of a cell
    enum Cell : std::uint_least8_t {
        Empty = 0, Tree, Tent
    };

    enum Direction : std::uint_least8_t {
	up = 0, right = 1, down = 2, left = 3
    };

    // All 4 directions
    static constexpr std::array<Direction, 4> directions = {up, right, down, left};

    // rotate clockwise
    static constexpr Direction rotate(Direction const d, unsigned const angle) {
        return static_cast<Direction>((static_cast<unsigned>(d) + angle) % 4);
    }

    // Coordinate
    struct Coo {
        std::size_t index, row, column;
    };

    // Tree-Tent pair with tree coordinate and relative tent location
    struct TT_pair {
	Coo tree;
        Direction tent;
    };

    // Constructors

    Tents() = default;

    explicit Tents(std::size_t const n, std::size_t const m) :
        n_(n), m_(m), cells_(n*m, Empty),
        number_rows_(n, 0), number_columns_(m, 0)
    {}


    // I/O

    friend std::istream& operator>>(std::istream& is, Tents& t) {
        t.read_tents(is);
        return is;
    }

    friend std::ostream& operator<<(std::ostream& os, Tents const& t) {
        t.write_tents(os);
        return os;
    }

    // Getter

    // Get number of rows (dimension n)
    std::size_t n() const noexcept {
        return n_;
    }

    // Get number of columns (dimension m)
    std::size_t m() const noexcept {
        return m_;
    }

    /* Get cell at index
     *
     * precondition: index is valid (index < n()*m())
     */
    Cell cell(std::size_t const index) const {
        assert(index < n_*m_);
        return cells_[index];
    }

    // Get cell at coo.index
    Cell cell(Coo const coo) const {
        return cell(coo.index);
    }

    /* Get cell at (row,column)
     *
     * precondition: valid row and column (row < n() && column < m())
     */
    Cell cell(std::size_t const row, std::size_t const column) const {
        assert(row < n_ && column < m_);
        return cells_[rc_to_index(row, column)];
    }

    /* Get number of tents in row row
     *
     * precondition: valid row (row < n())
     */
    std::size_t number_of_tents_in_row(std::size_t const row) const {
        assert(row < n_);
        return number_rows_[row];
    }

    /* Get number of tents in column column
     *
     * precondition: valid column (column < m())
     */
    std::size_t number_of_tents_in_column(std::size_t const column) const {
        assert(column < m_);
        return number_columns_[column];
    }

    // Get numer of tents in rows, unchecked
    std::size_t const* number_of_tents_in_rows() const noexcept {
        return number_rows_.data();
    }

    // Get number of tents in columns, unchecked
    std::size_t const* number_of_tents_in_columns() const noexcept {
        return number_columns_.data();
    }

    // Get indices of trees
    std::vector<std::size_t> const& indices_of_trees() const noexcept {
        return trees_;
    }

    // Get indices of tents
    std::vector<std::size_t> const& indices_of_tents() const noexcept {
        return tents_;
    }


    // Checks if this is a valid Tents-Puzzle
    bool is_valid_puzzle() const {
        return n_ && m_ && // number of rows and columns != 0
            // sum of number of tents in rows == number of trees
            std::accumulate(number_rows_.begin(), number_rows_.end(), 0ul) == trees_.size() &&
            // sum of number of tents in columns == number of trees
            std::accumulate(number_columns_.begin(), number_columns_.end(), 0ul) == trees_.size();
    }


    // Modifiers

    /* Add a tent
     *
     * precondition 1: index is valid (index < n()*m())
     * precondition 2: cell(index) is empty (cell(index) == Empty)
     */
    void add_tent(std::size_t const index) {
        assert(index < n_*m_); // precondition 1
        assert(cells_[index] == Empty); // precondition 2

        cells_[index] = Tent;
        tents_.push_back(index);
    }

    /* Add a tree together with a tent
     *
     * precondition 1: pair is valid
     * precondition 2: cell(pair.tree) and cell(tent_coo(pair)) are empty
     */
    void add_tt_pair(TT_pair const pair) {
        assert(valid_tt_pair(pair)); // precondition 1

        Coo const tent = tent_coo(pair);

        assert(cell(pair.tree) == Empty && cell(tent) == Empty); // precondition 2

        cells_[pair.tree.index] = Tree;
        trees_.push_back(pair.tree.index);

        cells_[tent.index] = Tent;
        tents_.push_back(tent.index);
        ++number_rows_[tent.row];
        ++number_columns_[tent.column];
    }

    /* Remove a tree together with a tent
     *
     * precondition 1: pair is valid
     * precondition 2: cell(pair.tree) == Tree && cell(tent_coo(pair)) == Tent
     */
    void remove_tt_pair(TT_pair const pair) {
        assert(valid_tt_pair(pair)); // precondition 1

        Coo const tent = tent_coo(pair);

        assert(cell(pair.tree) == Tree && cell(tent) == Tent); // precondition 2

        cells_[pair.tree.index] = Empty;
        trees_.erase(std::find(trees_.begin(), trees_.end(), pair.tree.index));

        cells_[tent.index] = Empty;
        tents_.erase(std::find(tents_.begin(), tents_.end(), tent.index));
        --number_rows_[tent.row];
        --number_columns_[tent.column];
    }

    // Remove all the tents
    void clear_tents() {
	for (std::size_t const tent : tents_)
	    cells_[tent] = Empty;

        tents_.clear();
    }


    // Coordinates

    // Checks if coo is valid
    bool valid_coo(Coo const coo) const noexcept {
        return coo.row < n_ && coo.column < m_ && coo.index == rc_to_index(coo.row, coo.column);
    }

    Coo index_to_coo(std::size_t const index) const noexcept {
        return Coo{index, index / m_, index % m_};
    }

    std::size_t rc_to_index(std::size_t const row, std::size_t const column) const noexcept {
        return row * m_ + column;
    }

    Coo rc_to_coo(std::size_t const row, std::size_t const column) const noexcept {
        return Coo{rc_to_index(row, column), row, column};
    }

    std::optional<Coo> up_coo(Coo const coo) const noexcept {
        return coo.row > 0 ? std::make_optional(rc_to_coo(coo.row - 1, coo.column))
                           : std::nullopt;
    }

    std::optional<Coo> right_coo(Coo const coo) const noexcept {
        return coo.column < m_-1 ? std::make_optional(rc_to_coo(coo.row, coo.column + 1))
                                 : std::nullopt;
    }

    std::optional<Coo> down_coo(Coo const coo) const noexcept {
        return coo.row < n_-1 ? std::make_optional(rc_to_coo(coo.row + 1, coo.column))
	                      : std::nullopt;
    }

    std::optional<Coo> left_coo(Coo const coo) const noexcept {
        return coo.column > 0 ? std::make_optional(rc_to_coo(coo.row, coo.column - 1))
	                      : std::nullopt;
    }

    std::optional<Coo> adj_coo(Coo const coo, Direction const d) const noexcept {
	switch (d) {
	case up:
	    return up_coo(coo);
	case right:
	    return right_coo(coo);
	case down:
	    return down_coo(coo);
        default: // left
	    return left_coo(coo);
	}
    }


    // TT_pairs

    // Checks if pair is valid
    bool valid_tt_pair(TT_pair const pair) const noexcept {
        return valid_coo(pair.tree) && adj_coo(pair.tree, pair.tent);
    }

    /* Get Coo of tent
     *
     * precondition: pair is valid
     */
    Coo tent_coo(TT_pair const pair) const {
        assert(valid_tt_pair(pair));
        return *adj_coo(pair.tree, pair.tent);
    }

private:
    std::size_t n_ {}, m_ {}; //n = number of rows, m = number of columns
    std::vector<Cell> cells_ {}; // all cells
    std::vector<std::size_t> number_rows_ {}, number_columns_ {}; // number of tents in row/column
    std::vector<std::size_t> trees_ {}; // indices of trees
    std::vector<std::size_t> tents_ {}; // indices of tents

    void read_tents(std::istream& is);
    void write_tents(std::ostream& os) const;
};

// Helper function for gen_tents and validate_tents, defined in validate_tents.cpp
bool surrounded_by_tents(Tents const& t, Tents::Coo coo);

// Generate Tents puzzle, defined in generate_tents.cpp
Tents gen_tents(std::size_t n, std::size_t m, unsigned short difficulty);
Tents gen_tents(std::size_t n, std::size_t m, unsigned short difficulty, std::uint32_t seed);

// Validate Tents solution, defined in validate_tents.cpp
bool validate_tents(Tents const& t);

#endif // TENTS_HPP
