/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENTS_HPP
#define ARGUMENTS_HPP

#include <string_view>
#include <iostream> // std::clog
#include <cstdint> // std::uint32_t
#include <cstddef> // std::size_t

struct arguments {
    // options
    bool verbose = false;
    bool input = false;
    bool output = false;
    bool unoptimised = false;
    bool time = false;
    bool cnf_output = false;
    bool read_solver_output = false;
    bool validate = false;
    bool generate = false;
    // files for the options
    // <option> && !<option_file> => read from stdin or write to stdout
    char const* input_file = nullptr;
    char const* read_solver_output_file = nullptr;
    char const* output_file = nullptr;
    char const* cnf_output_file = nullptr;
    char const* validate_file = nullptr;
    char const* generate_file = nullptr;
    // arguments for generating puzzles
    std::size_t size_m = 10;
    std::size_t size_n = 10;
    unsigned short difficulty = 5;
    bool use_seed = false;
    std::uint32_t seed = 0;
};

extern struct arguments arguments;

// Print str and newline to stderr if arguments.verbose is set
inline void verbose_output(std::string_view const str) {
    if (arguments.verbose)
        std::clog << str << std::endl;
}

/* parse command line arguments to struct arguments
 *
 * return value:
 *   -1 exit with EXIT_FAILURE (error occured)
 *   0 start the solver
 *   1 exit with EXIT_SUCCESS
 */
int parse_arguments(int argc, char* argv[]);

#endif // ARGUMENTS_HPP
