/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tents.hpp"
#include <iostream> // std::istream, std::ostream, std::cerr
#include <limits> // std::numeric_limits
#include <string>

void Tents::read_tents(std::istream& is) {
    if (!(is >> n_ >> m_)) {
        std::cerr << "Error reading number of rows and columns.\n";
        return;
    } else if (!n_ || !m_) {
        std::cerr << "Invalid number of rows or columns.\n";
        is.setstate(std::ios::failbit);
        return;
    }

    // reserve space
    cells_.reserve(n_*m_);
    number_rows_.reserve(n_);
    number_columns_.reserve(m_);

    // ignore until end of line
    is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::string row_str (m_, '\0');
    for (std::size_t row = 0; row < n_; ++row) {
        if (!is.read(row_str.data(), m_)) { // read row
            std::cerr << "Error reading row " << row << ".\n";
            return;
        }

        // assign cells
        for (std::size_t column = 0; column < m_; ++column)
            switch (row_str[column]) {
            case '.': // empty cell
                cells_.push_back(Empty);
                break;
            case 'T': // Tree
                cells_.push_back(Tree);
                trees_.push_back(rc_to_index(row, column));
                break;
            case 'X': // Tent
                cells_.push_back(Tent);
                tents_.push_back(rc_to_index(row, column));
                break;
            default: // Error
                std::cerr << "Error reading cell (" << row << ',' << column << ").\n";
                is.setstate(std::ios::failbit);
                return;
            }

        // number of tents in row
        std::size_t num;
        if (!(is >> num)) {
            std::cerr << "Error reading number of tents in row " << row << ".\n";
            return;
        }
        number_rows_.push_back(num);
        
        // ignore until end of line
        is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    // number of tents in columns
    for (std::size_t column = 0; column < m_; ++column) {
        std::size_t num;
        if (!(is >> num)) {
            std::cerr << "Error reading number of tents in column " << column << ".\n";
            return;
        }
        number_columns_.push_back(num);
    }
}

void Tents::write_tents(std::ostream& os) const {
    if (!is_valid_puzzle()) {
        os << "INVALID\n";
        return;
    }

    os << n_ << ' ' << m_ << '\n';

    for (std::size_t row = 0; row < n_; ++row) {
        // output row
        for (std::size_t index = rc_to_index(row, 0); index < rc_to_index(row, m_); ++index)
            switch (cells_[index]) {
            case Empty:
                os << '.';
                break;
            case Tree:
                os << 'T';
                break;
            case Tent:
                os << 'X';
            }
        
        // output number of tents in row
        os << ' ' << number_rows_[row] << '\n';
    }

    // output number of tents in columns
    for (std::size_t column = 0; column < m_-1; ++column)
        os << number_columns_[column] << ' ';

    os << number_columns_[m_-1] << '\n';
}
