/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMER_HPP
#define TIMER_HPP

#include <ctime> // std::clock_t, std::clock()
#include <cstddef> // std::size_t

extern std::clock_t gen_start;
extern std::clock_t gen_end;
extern std::clock_t solver_start;
extern std::clock_t solver_end;

inline void clock_gen_start() noexcept {
    gen_start = std::clock();
}

inline void clock_gen_end() noexcept {
    gen_end = std::clock();
}

inline void clock_solver_start() noexcept {
    solver_start = std::clock();
}

inline void clock_solver_end() noexcept {
    solver_end = std::clock();
}

// print csv line
void print_time_csv(std::size_t num_clauses, std::size_t num_vars, char const* input_file);

#endif // TIMER_HPP
