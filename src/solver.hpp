/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_HPP
#define SOLVER_HPP

#include "arguments.hpp" // arguments.verbose
#include <cadical.hpp> // CaDiCaL::Solver
#include <optional>
#include <string_view>
#include <sstream> // std::stringstream, std::ostream
#include <cstddef> // std::size_t

// Solver with CaDiCaL::Solver as base

class Solver {
public: 
    // Constructor: set number of variables, if save_cnf init sstr, if use_cadical init solver
    explicit Solver(std::size_t const num_var, bool const save_cnf = false, bool const use_cadical = true) :
        num_var(num_var)
    {
        if (use_cadical) {
            solver.emplace();

            // CaDiCaL output
            if (arguments.verbose)
                solver->prefix("CaDiCaL: ");
            else
                solver->set_long_option("--quiet");

            reserve();
        }

        if (save_cnf)
            sstr.emplace();
    }

    // Delete copy constructor and assignment, default move constructor and assignment
    Solver(Solver const&) = delete;
    Solver(Solver&&) = default;
    Solver& operator=(Solver const&) = delete;
    Solver& operator=(Solver&&) = default;

    // add variable with value to current clause
    void add_to_clause(std::size_t const index, bool const value) {
	int const i = value ? static_cast<int>(index+1) : -static_cast<int>(index+1);

        if (solver)
            solver->add(i);

        if (sstr)
            *sstr << i << ' ';
    }

    // terminate current clause
    void terminate_clause() {
        if (solver)
            solver->add(0);
        
        if (sstr)
            *sstr << "0\n";
        
        ++num_clauses;
    }

    // get a new variable, increment num_var
    std::size_t get_new_var() {
        return num_var++;
    }

    // reserve num_var, calling this function is not neccesarry but an optimization
    void reserve() {
        if (solver)
            solver->reserve(static_cast<int>(num_var));
    }

    // solve and returns if satisfiable, solver must be valid
    bool solve() {
	return solver->solve() == 10;
    }

    // get value of found satisfiable assignment, must be solved and satisfiable
    bool val(std::size_t const index) const {
	return solver->val(static_cast<int>(index+1)) > 0;
    }

    // Add comment to DIMACS
    void add_comment(std::string_view const str) {
        if (sstr)
            *sstr << "c " << str << '\n';
    }

    // Add comment to DIMACS if arguments.verbose
    void add_verbose_comment(std::string_view const str) {
        if (arguments.verbose)
            add_comment(str);
    }

    // write DIMACS to ostr, sstr must be valid
    std::ostream& write_dimacs(std::ostream& ostr) const {
        ostr << "p cnf " << num_var << ' ' << num_clauses << '\n';
        return ostr << sstr->rdbuf();
    }

    std::size_t get_num_clauses() const noexcept {
        return num_clauses;
    }

    std::size_t get_num_var() const noexcept {
        return num_var;
    }

private:
    mutable std::optional<CaDiCaL::Solver> solver {}; // for val()
    std::optional<std::stringstream> sstr {};
    std::size_t num_clauses {};
    std::size_t num_var;
};

#endif // SOLVER_HPP
