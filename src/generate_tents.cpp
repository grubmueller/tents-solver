/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tents.hpp" // Tents
#include "solver.hpp" // Solver
#include "cnf.hpp" // gen_cnf
#include "arguments.hpp" // verbose_output
#include <string> // std::to_string
#include <random> // std::minstd_rand, std::random_device, std::uniform_int_distribution
#include <algorithm> // std::max, std::remove
#include <cmath> // std::sqrt

namespace {
// Check if a tent is possible at tent_coo
inline bool tent_possible(Tents const& t, Tents::Coo const tent_coo) {
    return t.cell(tent_coo) == Tents::Empty && !surrounded_by_tents(t, tent_coo);
}

// Check if a valid TT_pair is possible
inline bool tt_pair_possible(Tents const& t, Tents::TT_pair const pair) {
    return t.cell(pair.tree) == Tents::Empty && tent_possible(t, t.tent_coo(pair));
}

// Get all possible TT_pairs in the neighbourhood of init
std::vector<Tents::TT_pair> possible_neighbourhood(Tents const& t, Tents::Coo const init) {
    std::vector<Tents::TT_pair> c_poss {};
    c_poss.reserve(26); // at most 26 possible TT_pairs

    /* Relative direction encoding:
     * only check possible tree locations north, north north and north east, then rotate the compass (repeat for each of the four directions)
     * all cells in a diamond shape around init will be checked
     *
     * direction names used in the following: n -> north, e -> east, s -> south, w -> west
     */
    for (Tents::Direction const north : Tents::directions)
	if (auto const n = t.adj_coo(init, north); n) { // cell north exists
            Tents::Direction const east = Tents::rotate(north, 1);

            // helper lambda, return TT_pair if possible, otherwise std::nullopt
            auto const tt_pair_if_poss = [&t] (Tents::Coo const tree, Tents::Direction const tent) {
                auto const tent_coo = t.adj_coo(tree, tent);
                return tent_coo && tent_possible(t, *tent_coo)
                        ? std::optional<Tents::TT_pair>{{tree, tent}}
                        : std::nullopt;
            };

	    if (t.cell(*n) == Tents::Empty) // Possible tree in n
                // d = north, east, west -> nn, ne, nw possible tent locations
                for (Tents::Direction const d : {north, east, Tents::rotate(north, 3)})
                    if (auto const nd = tt_pair_if_poss(*n, d); nd)
                        c_poss.push_back(*nd);

            // Possible tree in nn
            if (auto const nn = t.adj_coo(*n, north); nn && t.cell(*nn) == Tents::Empty)
                // all directions -> nnn, nne, nns(=n), nnw possible tent locations
                for (Tents::Direction const d : Tents::directions)
                    if (auto const nnd = tt_pair_if_poss(*nn, d); nnd)
                        c_poss.push_back(*nnd);

            // Possible tree in ne
            if (auto const ne = t.adj_coo(*n, east); ne && t.cell(*ne) == Tents::Empty)
                // all directions -> nen, nee, nes, new(=n) possible tent locations
                for (Tents::Direction const d : Tents::directions)
                    if (auto const ned = tt_pair_if_poss(*ne, d); ned)
                        c_poss.push_back(*ned);
	}

    return c_poss;
}

// fill neighbourhood of init
void fill_neighbourhood(Tents& t, Tents::Coo const init, std::minstd_rand& gen, std::size_t const branches, std::size_t const r_depth) {
    std::vector<Tents::TT_pair> p_poss = possible_neighbourhood(t, init);

    for (std::size_t i = 0; i < branches; ++i) {
        if (i > 0) // We have to clean p_poss, some of the TT_pairs might not be possible anymore
            p_poss.erase(std::remove_if(p_poss.begin(), p_poss.end(),
                            [&t] (Tents::TT_pair const pair) {return !tt_pair_possible(t, pair);}),
                    p_poss.end());

        if (p_poss.empty()) // no possible neighbourhood
            return;

        // Get random pair from neighbourhood
        std::uniform_int_distribution<std::size_t> uid_neighbourhood {0, p_poss.size() - 1};
        Tents::TT_pair const pair = p_poss[uid_neighbourhood(gen)];

        t.add_tt_pair(pair);

        if (r_depth) // recursively fill the neighbourhood
            fill_neighbourhood(t, pair.tree, gen, branches, r_depth - 1);
    }
}

// Try to make a puzzle with a unique solution, returns if successful
bool try_make_unique(Tents& t, std::minstd_rand& gen, std::size_t max_tries) {
    verbose_output("Checking if generated puzzle has a unique solution ...");

    Tents t_without_tents = t;
    t_without_tents.clear_tents();

    Solver s = gen_cnf(t_without_tents);

    // Add tents to s as clause -tent1 -tent2 ...
    for (std::size_t const tent : t.indices_of_tents())
        s.add_to_clause(tent, false);

    s.terminate_clause();

    verbose_output("Solving for second solution ...");

    if (s.solve()) {
        verbose_output("Generated puzzle does not have a unique solution!");

        if (!max_tries)
            return false;

        // Another solution found, try to make t unique

        // Get Coo of tent that is placed elsewhere
        Tents::Coo faulty_coo {};

        for (std::size_t i = 0; i < t.n()*t.m(); ++i)
            if (s.val(i) && t.cell(i) != Tents::Tent) {
                faulty_coo = t.index_to_coo(i);
                break;
            }

        assert(t.cell(faulty_coo) == Tents::Empty);

        // Try to place a tree there
        verbose_output("Trying to place a new tree ...");

        std::vector<Tents::TT_pair> pairs {};

        for (Tents::Direction const dir : Tents::directions)
            if (auto const tent = t.adj_coo(faulty_coo, dir); tent && tent_possible(t, *tent))
                pairs.push_back({faulty_coo, dir});

        if (pairs.empty()) { // Cannot place a tree there
            verbose_output("Cannot place a new tree!");
            verbose_output("Trying to remove a tree ...");

            // Find tree that correspond to faulty_coo
            std::optional<Tents::Coo> remove_tree {};

            for (Tents::Direction const dir : Tents::directions)
                if (auto const tree = t.adj_coo(faulty_coo, dir); tree && t.cell(*tree) == Tents::Tree) {
                    if (!remove_tree) {
                        remove_tree = tree;
                    } else { // multiple possible trees
                        remove_tree.reset();
                        break;
                    }
                }

            if (!remove_tree) { // multiple possible trees
                verbose_output("Cannot remove a tree!");
                return false;
            }

            // Find tent in t that should correspond to remove_tree (reuse pairs)
            for (Tents::Direction const dir : Tents::directions)
                if (auto const tent = t.adj_coo(*remove_tree, dir); tent && t.cell(*tent) == Tents::Tent)
                    pairs.push_back({*remove_tree, dir});

            if (pairs.size() != 1) { // multiple possible tents
                verbose_output("Cannot remove a tree!");
                return false;
            }

            t.remove_tt_pair(pairs[0]);

            verbose_output("Removed a tree!");

            return try_make_unique(t, gen, max_tries-1);
        } else { // pairs is non-empty, add new TT_pair
            std::uniform_int_distribution<std::size_t> uid_directions_special {0, pairs.size() - 1};
            t.add_tt_pair(pairs[uid_directions_special(gen)]);

            verbose_output("Placed a new tree!");

            return try_make_unique(t, gen, max_tries-1);
        }
    }

    return true;
}
}


Tents gen_tents(std::size_t const n, std::size_t const m, unsigned short const difficulty) {
    // prepare seed generation
    std::random_device rd {};
    return gen_tents(n, m, difficulty, rd());
}

/* function gen_tents:
 * Input: n (number of rows), m (number of columns), difficulty (difficulty parameter; integer from 1 to 10), seed (randomization seed)
 * Output: Randomly generated tents puzzle with n rows, m columns of difficulty difficulty and according to seed
 *
 * functional principle:
 * at first randomly generate several anchor points; the exact amount is decided by difficulty; an anchor point is a tree-tent pair
 * then, starting from a randomly chosen anchor point, fill up the neighbourhood with tree-tent pairs.
 * afterwards choose a new random anchor point and fill up the neighbourhood from there
 * do until there are no anchor points left or all of them are no possible tree-tent pair any more
 *
 * precondition: n > 0 && m > 0 && 1 <= difficulty <= 10
 */
Tents gen_tents(std::size_t const n, std::size_t const m, unsigned short const difficulty, std::uint32_t const seed) {
    verbose_output("Generating Tents puzzle with n=" + std::to_string(n) + ", m=" + std::to_string(m)
        + ", difficulty " + std::to_string(difficulty) + " and seed " + std::to_string(seed) + " ...");

    assert(n > 0 && m > 0 && difficulty > 0 && difficulty <= 10); // precondition

    std::size_t const nm = n * m;
    Tents t {n, m};

    if (nm == 1) // trivial 1x1 puzzle
	return t;

    // Difficulty
    std::size_t const sqrt_10 = static_cast<std::size_t>(std::sqrt(nm) / 10.0);

    // (minimal) parameters for fill_neighbourhood
    std::size_t r_depth = sqrt_10 / 2;
    std::size_t branches = (sqrt_10 + 1) / 2;

    std::size_t init_pos_count; // number of anchor points

    if (nm < 100) { // small puzzles: < 10x10
        r_depth += (difficulty + 3)/5; // 1 -> +0, 2,..,6 -> +1, 7,..,10 -> +2
        branches += difficulty/4; // 1,..,3 -> +0, 4,..,7 -> +1, 8,..,10 -> +2

        constexpr double init_percentage_min = 0.1;
        constexpr double init_percentage_max = 0.3;
        double const init_percentage = difficulty <= 2 ? init_percentage_min
            : init_percentage_min + (difficulty-2)/8.0 * (init_percentage_max - init_percentage_min);

        init_pos_count = std::max<std::size_t>(1,
                static_cast<std::size_t>(static_cast<double>(nm) * init_percentage));
    } else if (difficulty <= 3) { // big puzzles and difficulty <= 3
        if (difficulty >= 2)
            r_depth += 1;

        if (difficulty == 3)
            branches += 1;

        init_pos_count = 10;
    } else if (difficulty  <= 8) { // big puzzles and 4 <= difficulty <= 8
        r_depth += difficulty >= 6 ? 2 : 1;
        branches += difficulty >= 7 ? 2 : 1;

        constexpr std::size_t init_pos_min = 20;
        constexpr std::size_t init_pos_max = 40;

        init_pos_count = init_pos_min + (init_pos_max - init_pos_min) * (difficulty - 4) / 4;
    } else { // big puzzles and difficulty >= 9
        r_depth += difficulty == 10 ? 3 : 2;
        branches += difficulty == 10 ? 3 : 2;

        init_pos_count = std::max<std::size_t>(50, nm / 3 / sqrt_10);
    }

    verbose_output("Using parameters r_depth=" + std::to_string(r_depth) +
            ", branches=" + std::to_string(branches) +
            "and init_pos_count=" + std::to_string(init_pos_count));
    

    std::vector<Tents::TT_pair> init_pos {};
    init_pos.reserve(init_pos_count);
    
    std::minstd_rand gen {seed}; // set generator

    { // get some random initial anchor points (initial positions)
        std::uniform_int_distribution<std::size_t> uid_cells {0, nm - 1};

        // Reserve space
        std::vector<Tents::TT_pair> pairs {};
        pairs.reserve(4);

        for (std::size_t i = 0; i < init_pos_count; ++i) {
            Tents::Coo const tree = t.index_to_coo(uid_cells(gen));

            for (Tents::Direction const dir : Tents::directions)
                if (t.adj_coo(tree, dir))
                    pairs.push_back({tree, dir});

            // pairs is always non-empty
            std::uniform_int_distribution<std::size_t> uid_directions_special {0, pairs.size() - 1};
            init_pos.push_back(pairs[uid_directions_special(gen)]);
            
            pairs.clear();
        }
    }

    // shuffle init_pos
    std::shuffle(init_pos.begin(), init_pos.end(), gen);

    // starting from an anchor point, fill the neighbourhood with tree-tent pairs
    for (Tents::TT_pair const pair : init_pos)
        // init_pos only contains valid TT_pairs
	if (tt_pair_possible(t, pair)) {
	    t.add_tt_pair(pair);
	    fill_neighbourhood(t, pair.tree, gen, branches, r_depth);
	}


    // Try to make a unique puzzle
    if (!try_make_unique(t, gen, sqrt_10)) {
        // Generate new puzzle with new seed, all values of std::uint32_t are possible
	std::uniform_int_distribution<std::uint32_t> next_seed {0};
	return gen_tents(n, m, difficulty, next_seed(gen));
    }

    // finally remove all the tents and return t
    t.clear_tents();
    return t;
}
