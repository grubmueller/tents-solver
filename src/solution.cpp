#include "cnf.hpp"
#include "tents.hpp" // Tents, validate_tents
#include "solver.hpp" // Solver
#include "arguments.hpp" // verbose_output
#include <iostream> // std::istream, std::cerr
#include <string> // std::string, std::getline
#include <limits> // std::numeric_limits

namespace {
// Read all values in line and add tents to t
bool read_values_in_line(Tents& t, std::istream& is) {
    for (std::ptrdiff_t val; is.peek() != '\n' && is >> val;)
        if (val > 0 && val <= static_cast<std::ptrdiff_t>(t.n()*t.m())) { // val-1 is index of tent
            if (std::size_t const index = static_cast<std::size_t>(val - 1); t.cell(index) == Tents::Empty) {
                t.add_tent(index);
            } else if (t.cell(index) == Tents::Tree) { // Error
                std::cerr << "Error adding a tent: There is a tree in the cell.\n";
                return false;
            }
        }

    is.ignore(); // ignore end of line
    return true;
}
}

// Add tents to t from s, s must be solved and satisfiable
void add_tents_from_solver(Tents& t, Solver const& s) {
    for (std::size_t index = 0; index < t.n()*t.m(); ++index)
        if (s.val(index) && t.cell(index) != Tents::Tent)
            t.add_tent(index);
}

/* Add tents to t from a solver output
 *
 * format:
 * lines beginning with "c " are comments and ignored
 * empty lines are ignored
 * lines "s SATISFIABLE", "SAT" and "SATISFIABLE" mean SAT
 * lines beginning with "s " and not "s SATISFIABLE" mean UNSAT
 * lines "UNSAT" and "UNSATISFIABLE" mean UNSAT
 * lines beginning with "v " or lines starting with a number or - contain the values of a solution
 *
 * return value:
 * true if the output is satisfiable and the resulting Tents solution is valid
 * false otherwise
 */
bool add_tents_from_solver_output(Tents& t, std::istream& is) {
    for (char line_begin; is.get(line_begin);)
        switch (line_begin) {
        case 'c': // comment, ignore until end of line
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            break;
        case 's':
            is.ignore(); // ignore space

            if (std::string solution; std::getline(is, solution) && solution != "SATISFIABLE") {
                // Not satisfiable
                std::cerr << "Solver did not find a satisfiable assignment!\n";
                return false;
            }

            break;
        case 'v':
            if (!read_values_in_line(t, is)) // read all values in line
                return false;

            break;
        case '-':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            is.unget(); // unget line_begin

            if (!read_values_in_line(t, is)) // read all values in line
                return false;

            break;
        default:
            is.unget(); // unget line_begin

            if (std::string line; std::getline(is, line) &&
                    (line.empty() || line == "SAT" || line == "SATISFIABLE")) {
                break; // empty or satisfiable, read other lines
            } else if (line == "UNSAT" || line == "UNSATISFIABLE") {
                // Not satisfiable
                std::cerr << "Solver did not find a satisfiable assignment!\n";
                return false;
            } else { // unknown line
                std::cerr << "Error reading solver output, unknown line: " << line << ".\n";
                return false;
            }
        }

    if (!is.eof()) {
        std::cerr << "Error reading solver output!\n";
        return false;
    }

    verbose_output("Validating solution ...");

    if (!validate_tents(t)) {
        std::cerr << "Validation failed! Solver output is not a valid solution!\n";
        return false;
    }

    return true;
}
