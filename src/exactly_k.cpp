/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cnf.hpp"
#include "solver.hpp" // Solver
#include "arguments.hpp" // verbose_output, arguments.verbose, arguments.unoptimised
#include <string> // std::string, std::to_string
#include <algorithm> // std::min, std::max
#include <cmath> // std::log2, std::ceil, std::sqrt
#include <cassert>

void binomial(Solver& s, std::size_t nk, bool val, std::vector<std::size_t> const& elements);
void binary(Solver& s, std::size_t k, bool val, std::vector<std::size_t> const& elements);
void sequential(Solver& s, std::size_t k, bool val, std::vector<std::size_t> const& elements);

namespace {
// Add clauses to s, such that at least k of elements must be true, and use binomial encoding
inline void at_least_k_binomial(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    // at least k true == for every (elements.size()-k+1)-subset there is a true element
    binomial(s, elements.size()-k+1, true, elements);
}

// Add clauses to s, such that at least k of elements must be true, and use binary encoding
inline void at_least_k_binary(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    // at least k true == at most elements.size()-k false
    binary(s, elements.size()-k, false, elements);
}

// Add clauses to s, such that at least k of elements must be true, and use sequential encoding
inline void at_least_k_sequential(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    // at least k true == at most elements.size()-k false
    sequential(s, elements.size()-k, false, elements);
}

// Add clauses to s, such that at most k of elements can be true, and use binomial encoding
inline void at_most_k_binomial(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    // at most k true == for every (k+1)-subset there is a false element
    binomial(s, k+1, false, elements);
}

// Add clauses to s, such that at most k of elements can be true, and use binary encoding
inline void at_most_k_binary(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    binary(s, k, true, elements);
}

// Add clauses to s, such that at most k of elements can be true, and use sequential encoding
inline void at_most_k_sequential(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    sequential(s, k, true, elements);
}

/* Check if we should use at_most_k_binomial with given k, n
 *
 * Reference: Bittner et al.: SAT Encodings of the At-Most-k Constraint
 * https://www.isf.cs.tu-bs.de/cms/team/thuem/papers/2019-SEFM-Bittner.pdf
 * Function selective, page 4, and k_binom, page 13
 */
inline bool use_at_most_k_binomial(std::size_t const k, std::size_t const n) {
    if (n < 6) // For n < 6, binomial encoding is always used
        return true;
    else if (n < 40)
        return k >= n-2;
    else
        return k == n-1;
}

// Check if we should use at_least_k_binomial with given k, n
inline bool use_at_least_k_binomial(std::size_t const k, std::size_t const n) {
    return use_at_most_k_binomial(n-k, n);
}

/* Check if we should use at_most_k_binary (after checking use_at_most_k_binomial) with given k, n
 *
 * Reference: Bittner et al.: SAT Encodings of the At-Most-k Constraint
 * https://www.isf.cs.tu-bs.de/cms/team/thuem/papers/2019-SEFM-Bittner.pdf
 * Function selective, page 4, and k_split, page 13
 */
inline bool use_at_most_k_binary(std::size_t const k, std::size_t const n) {
    double const nd = static_cast<double>(n);
    double const log = std::ceil(std::log2(nd));
    double const a = 1 + 2*log;
    double const b = 2*(log*(nd+1) - 2*nd + 5);

    return static_cast<double>(k) > (b + std::sqrt(b*b - 4*a))/(2*a);
}

// Check if we should use at_least_k_binary (after checking use_at_least_k_binomial) with given k, n
inline bool use_at_least_k_binary(std::size_t const k, std::size_t const n) {
    return use_at_most_k_binary(n-k, n);
}
}

/* Add clauses to s, such that exactly k of elements must be true
 *
 * precondition: k <= elements.size()
 */
void exactly_k(Solver& s, std::size_t const k, std::vector<std::size_t> const& elements) {
    // assert precondition
    assert(k <= elements.size());

    if (k == 0) {
        // Each element must be false
        for (std::size_t const element : elements) {
            s.add_to_clause(element, false);
            s.terminate_clause();
        }
    } else if (k == elements.size()) {
        // Each element must be true
        for (std::size_t const element : elements) {
            s.add_to_clause(element, true);
            s.terminate_clause();
        }
    } else { // 0 < k < elements.size()
        // at least k
        if (arguments.unoptimised || use_at_least_k_binomial(k, elements.size()))
            at_least_k_binomial(s, k, elements);
        else if (use_at_least_k_binary(k, elements.size()))
            at_least_k_binary(s, k, elements);
        else
            at_least_k_sequential(s, k, elements);

        // at most k
        if (arguments.unoptimised || use_at_most_k_binomial(k, elements.size()))
            at_most_k_binomial(s, k, elements);
        else if (use_at_most_k_binary(k, elements.size()))
            at_most_k_binary(s, k, elements);
        else
            at_most_k_sequential(s, k, elements);
    }
}

/* For every nk-subset I of elements, add a clause which sets every i in I to val
 * e.g. nk=2, elements={1,2,3}, val=true -> (1 OR 2) AND (1 OR 3) AND (2 OR 3)
 * e.g. nk=2, elements={4,5,6}, val=false -> (-4 OR -5) AND (-4 OR -6) AND (-5 OR -6)
 *
 * precondition: 0 < nk <= elements.size()
 */
void binomial(Solver& s, std::size_t const nk, bool const val, std::vector<std::size_t> const& elements) {
    std::size_t const n = elements.size();

    if (arguments.verbose) { // verbose output
        std::string const verbose_str = "binomial encoding with nk=" + std::to_string(nk) + " and n=" + std::to_string(n);
        verbose_output(verbose_str);
        s.add_verbose_comment(verbose_str);
    }

    // assert precondition
    assert(0 < nk && nk <= n);


    // fill indices with 0,..,nk-1
    std::vector<std::size_t> indices {};
    indices.reserve(nk);
    for(std::size_t i = 0; i < nk; ++i)
        indices.push_back(i);

    while (true) {
        // Add elements to clause
        for (std::size_t const index : indices)
            s.add_to_clause(elements[index], val);

        s.terminate_clause();

        // Determine if we should stop
        if (indices[0] == n-nk)
            return;

        // Shift indices
        for (std::size_t s = nk; s --> 0;) // s goes to 0, i.e. nk-1,..,0
            if (indices[s] < n-nk+s) {
                // This index can be shifted
                indices[s] += 1;

                // Shift higher indices to the beginning
                for (std::size_t i = s+1; i < nk; ++i)
                    indices[i] = indices[i-1]+1;

                break; // break for-loop
            }
    }
}

/* Binary encoding, introduced by Alan M. Frisch and Paul A. Giannaros
 * Reference: Frisch, A.M., Giannaros, P.A.: SAT Encodings of the At-Most-k Constraint.
 * http://www.it.uu.se/research/group/astra/ModRef10/papers/Alan%20M.%20Frisch%20and%20Paul%20A.%20Giannoros.%20SAT%20Encodings%20of%20the%20At-Most-k%20Constraint%20-%20ModRef%202010.pdf
 *
 * Add clauses to s, such that at most k of elements can have value val
 *
 * precondition: 0 < k < elements.size()
 */
void binary(Solver& s, std::size_t const k, bool const val, std::vector<std::size_t> const& elements) {
    std::size_t const n = elements.size();
    std::size_t const log = static_cast<std::size_t>(std::ceil(std::log2(n)));

    if (arguments.verbose) { // verbose output
        std::string const verbose_str = "binary encoding with k=" + std::to_string(k) + " and n=" + std::to_string(n);
        verbose_output(verbose_str);
        s.add_verbose_comment(verbose_str);
    }

    // assert precondition
    assert(0 < k && k < n);


    /* associate each element with a unique bit string s_i
     * associate elements[i] with bitstrings[i*log, (i+1)*log)
     * bitstrings[i*log + j] := j-th bit of the bitstring associated with elements[i]
     */
    std::vector<bool> bitstrings (n*log, false);

    // fill bitstrings with binary representation of index
    for (std::size_t i = 0; i < n; ++i)
        // j == index in bitstrings[i*log, (i+1)*log), k == i >> j
        for (std::size_t j = 0, k = i; k != 0; ++j, k >>= 1)
            if (k & 1) // check if the j-th bit of i is set
                bitstrings[i*log + j] = true;


    // add k*log variables for the B_(g,j), b[g*log + j] := B_(g,j)
    std::vector<std::size_t> b {};
    b.reserve(k*log);

    // fill b
    for (std::size_t i = 0; i < k*log; ++i)
        b.push_back(s.get_new_var());

    s.reserve(); // reserve new variables


    // reserve space for the tseitin variables
    std::vector<std::size_t> t {};
    t.reserve(k);

    for(std::size_t i = 0; i < n; ++i) {
        // Calculate the upper and lower bounds for the conjuctions and disjunctions 
        // Caution: We cannot use std::max(k - n + i, 0) due to size_t underflow
        std::size_t const lower = std::max(k + i, n) - n;
        std::size_t const upper = std::min(i+1, k);
        assert(lower < upper);

        std::size_t const g_max = upper - lower;

        // add new tseitin variables T_(g,i)
        t.reserve(g_max);
        for(std::size_t g = 0; g < g_max; ++g)
            t.push_back(s.get_new_var());

        s.reserve(); // reserve new variables


        /* add first disjunction (¬x_i V(g=lower to g=upper) T_(g,i))
         * if elements[i] is val, one of the tseitin variables must be true
         * i.e. for val==true: elements[i] => T_(lower,i) OR ... OR T_(upper-1,i)
         */
        s.add_to_clause(elements[i], !val);

        for(std::size_t const tseitin_var : t)
            s.add_to_clause(tseitin_var, true);

        s.terminate_clause();


        /* if T_(g,i) is true, [B_(g,0), B_(g,log)) must be equal to the bit string s_i
         * phi(i,g,j) is B_(g,j) if the j-th bit s_i is set, otherwise ¬B_(g,j)
         * So we need T_(g,i) => (phi(i,g,0) AND ... AND phi(i,g,log-1))
         * i.e. (¬T_(g,i) OR phi(i,g,0)) AND ... AND (¬T_(g,i) OR phi(i,g,log-1))
         */
        for(std::size_t g = lower; g < upper; ++g)
            for(std::size_t j = 0; j < log; ++j) {
                // add the second disjunction ¬T_(g, i) OR phi(i,g,j)
                s.add_to_clause(t[g-lower], false); // ¬T_(g,i)
                s.add_to_clause(b[g*log + j], bitstrings[i*log + j]); // phi(i,g,j)
                s.terminate_clause();
            }

        // clear tseitin variables
        t.clear();
    }
}

/* Sequential counter encoding, introduced by Carsten Sinz.
 * Reference: Carsten Sinz. Towards an Optimal CNF Encoding of Boolean Cardinality Constraints
 * http://www.carstensinz.de/papers/CP-2005.pdf
 *
 * Add clauses to s, such that at most k of elements can have value val
 *
 * precondition: 0 < k < elements.size()
 */
void sequential(Solver& s, std::size_t const k, bool const val, std::vector<std::size_t> const& elements) {
    std::size_t const n = elements.size();

    if (arguments.verbose) { // verbose output
        std::string const verbose_str = "sequential encoding with k=" + std::to_string(k) + " and n=" + std::to_string(n);
        verbose_output(verbose_str);
        s.add_verbose_comment(verbose_str);
    }

    // assert precondition
    assert(0 < k && k < n);


    /* introduce unary numbers s_0,..,s_{n-2} representing the partial sum
     * s_i = sum from j=0 to i x_j
     * where x_i <-> elements[i] is val
     *
     * representation: partial_sum[i*k + j] := j-th bit of unary number s_i
     */
    std::vector<std::size_t> partial_sum {};
    partial_sum.reserve((n-1)*k);

    // fill partial_sum with new variables
    for (std::size_t i = 0; i < (n-1)*k; ++i)
        partial_sum.push_back(s.get_new_var());

    s.reserve(); // reserve new variables

    /* If elements[i] is val, then the first bit of s_i must be set
     * i.e. for val==true: elements[i] => partial_sum[i*k]
     */
    for (std::size_t i = 0; i < n-1; ++i) {
        s.add_to_clause(elements[i], !val);
        s.add_to_clause(partial_sum[i*k], true);
        s.terminate_clause();
    }

    // The other bits of s_0 cannot be set
    for (std::size_t j = 1; j < k; ++j) {
        s.add_to_clause(partial_sum[j], false);
        s.terminate_clause();
    }

    /* If the j-th bit of s_{i-1} is set, the j-th bit of s_i must be set
     * i.e. partial_sum[(i-1)*k + j] => partial_sum[i*k + j]
     */
    for (std::size_t i = 1; i < n-1; ++i)
        for (std::size_t j = 0; j < k; ++j) {
            s.add_to_clause(partial_sum[(i-1)*k + j], false);
            s.add_to_clause(partial_sum[i*k + j], true);
            s.terminate_clause();
        }

    /* Add x_i to s_i
     * If elements[i] is val and the (j-1)-th bit of s_{i-1} is set, the j-th bit of s_i must be set
     * i.e. for val==true: (elements[i] AND partial_sum[(i-1)*k + (j-1)]) => partial_sum[i*k + j]
     * or as a clause: ¬elements[i] OR ¬partial_sum[(i-1)*k + (j-1)] OR partial_sum[i*k + j]
     */
    for (std::size_t i = 1; i < n-1; ++i)
        for (std::size_t j = 1; j < k; ++j) {
            s.add_to_clause(elements[i], !val);
            s.add_to_clause(partial_sum[(i-1)*k + (j-1)], false);
            s.add_to_clause(partial_sum[i*k + j], true);
            s.terminate_clause();
        }

    /* If elements[i] is val, then the last bit of s_{i-1} must not be set
     * i.e. for val==true: elements[i] => ¬partial_sum[i*k - 1]
     */
    for (std::size_t i = 1; i < n; ++i) {
        s.add_to_clause(elements[i], !val);
        s.add_to_clause(partial_sum[i*k - 1], false);
        s.terminate_clause();
    }
}
