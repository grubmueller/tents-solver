/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cnf.hpp" // exactly_k, std::vector, std::size_t
#include "tents.hpp" // Tents
#include "solver.hpp" // Solver
#include "arguments.hpp" // arguments.unoptimised, verbose_output
#include <map>
#include <cassert>

/* Function gen_cnf:
 *
 * input: an instance of Tents (which must be valid, see precondition)
 *
 * output: an instance of Solver containing the CNF corresponding to the input
 *
 * precondition: t.is_valid_puzzle()
 */
Solver gen_cnf(Tents const& t, bool const save_cnf, bool const use_cadical) {
    // assert precondition: sum of numbers in rows == sum of numbers in cols == trees.size()
    assert(t.is_valid_puzzle());

    // Information from t
    std::size_t const n = t.n(), m = t.m();
    std::size_t const nm = n*m;
    std::vector<std::size_t> const& trees {t.indices_of_trees()};
    std::vector<std::size_t> const& tents {t.indices_of_tents()};

    // Solver
    Solver s {nm, save_cnf, use_cadical};

    // Add already known tents
    if (!tents.empty()) {
        verbose_output("There are already known tents! Adding these to the CNF ...");
        s.add_comment("Already known tents");

        for (std::size_t const index : tents) {
            // if tent in index was misplaced, this clause will cause a contradiction and force UNSAT
            s.add_to_clause(index, true);
            s.terminate_clause();
        }
    }

    /* n×m matrix modelling if at position (m,n) a tent can be placed (true) or not (false).
     * Initially all elements are set to false
     */
    std::vector<bool> t_poss (nm, false);

    if (arguments.unoptimised) {
        /* Do not use the optimizations, just classify everything except trees as potentially
         * containing a tent
         */
        for (std::size_t index = 0; index < nm; ++index)
            if (t.cell(index) != Tents::Tree)
                t_poss[index] = true;
    } else {
        /* for every cell with a tree classify the horizontally and vertically adjacent cells as
         * potentially containing a tent, if they don't contain a tree
         */
        for (std::size_t const index : trees) {
            Tents::Coo const coo = t.index_to_coo(index);

            // Go through all directions (u, r, d, l)
            for (Tents::Direction const dir : Tents::directions)
                if (auto const dir_coo = t.adj_coo(coo, dir); dir_coo && t.cell(*dir_coo) != Tents::Tree)
                    t_poss[dir_coo->index] = true;
        }


        /* remove classification for cells adjacent to already known tents
         * no tents are allowed in cells adjacent to already known tents
        */
        for (std::size_t const index : tents) {
            Tents::Coo const coo = t.index_to_coo(index);

            // Set all directions (clockwise) to false

            if (auto const u = t.up_coo(coo); u) {
                if (auto const ul = t.left_coo(*u); ul)
                    t_poss[ul->index] = false;

                t_poss[u->index] = false;

                if (auto const ur = t.right_coo(*u); ur)
                    t_poss[ur->index] = false;
            }

            if (auto const r = t.right_coo(coo); r)
                t_poss[r->index] = false;

            if (auto const d = t.down_coo(coo); d) {
                if (auto const dr = t.right_coo(*d); dr)
                    t_poss[dr->index] = false;

                t_poss[d->index] = false;

                if (auto const dl = t.left_coo(*d); dl)
                    t_poss[dl->index] = false;
            }

            if (auto const l = t.left_coo(coo); l)
                t_poss[l->index] = false;
        }


        // remove classification for cells in rows labelled with 0
        for (std::size_t row = 0; row < n; ++row) {
            if (t.number_of_tents_in_row(row)) // row is not labelled with 0
                continue;

            for (std::size_t col = 0; col < m; ++col)
                t_poss[t.rc_to_index(row, col)] = false;
        }

        // remove classification for cells in columns labelled with 0
        for (std::size_t col = 0; col < m; ++col) {
            if (t.number_of_tents_in_column(col)) // col is not labelled with 0
                continue;

            for (std::size_t row = 0; row < n; ++row)
                t_poss[t.rc_to_index(row, col)] = false;
        }
    }


    // CNF: If no tent is possible in index, add clause -index
    verbose_output("Adding impossible tent locations to CNF ...");
    s.add_comment("Here no tents are possible");

    for (std::size_t index = 0; index < nm; ++index)
        if (!t_poss[index]) {
	    s.add_to_clause(index, false);
	    s.terminate_clause();
	}


    /* CNF: no two tents are allowed in adjacent cells
     *
     * traverse through t_poss from 0 to nm-1 excluding pairs of adjacent cells that could both hold
     * a tent (i. e. by adding the clause "-x -y 0\n").
     * Given a fixed index i for a possible tent, only exclude pairs (i, r), (i, dr), (i, d),
     * (i, dl) (and do so explicitly only if they can contain a tent).  Directions ul, u, ur, l need
     * not be checked, since they would have already been excluded at a previous given index.
     */

    verbose_output("Adding rule to CNF: No two tents are allowed in adjacent cells ...");
    s.add_comment("No two tents are allowed in adjacent cells");

    for (std::size_t index = 0; index < nm; ++index) {
        if (!t_poss[index]) // Here no tent is possible
            continue;

        // Helper lambda vor exclude pair (index, other)
        auto const exclude = [&s, index] (std::size_t const other) {
            s.add_to_clause(index, false);
            s.add_to_clause(other, false);
            s.terminate_clause();
        };

        Tents::Coo const coo = t.index_to_coo(index);

        if (auto const r = t.right_coo(coo); r && t_poss[r->index]) // Possible tent r
            exclude(r->index);

        if (auto const d = t.down_coo(coo); d) {
            if (auto const dr = t.right_coo(*d); dr && t_poss[dr->index]) // Possible tent dr
                exclude(dr->index);

            if (t_poss[d->index]) // Possible tent d
                exclude(d->index);

            if (auto const dl = t.left_coo(*d); dl && t_poss[dl->index]) // Possible tent dl
                exclude(dl->index);
        }
    }


    { // CNF: 1:1 relation between tents and trees
        verbose_output("Adding rule to CNF: 1:1 relation between tents and trees ...");
        s.add_comment("1:1 relation between tents and trees");

        /* data structure for accessing relation variables
         * valid variables are != 0 (0 can't be a new variable):
         * relation[index][direction] != 0 <=> possible tent in direction
         * If the variable is valid:
         * relation[index][direction] <-> tree at index has a relation with the tent in direction
         */
        std::map<std::size_t, std::size_t[4]> relation {};

        // Fill relation with new variables
        for (std::size_t const index : trees) {
            Tents::Coo const coo = t.index_to_coo(index);

            // Go through all directions (u, r, d, l)
            for (Tents::Direction const dir : Tents::directions)
                if (auto const dir_coo = t.adj_coo(coo, dir); dir_coo && t_poss[dir_coo->index])
                    relation[index][dir] = s.get_new_var(); // possible tent in direction dir

            if (!relation.count(index)) { // no possible tent location -> UNSAT
                verbose_output("No possible tent location around tree!");
                s.add_comment("No possible tent location around tree");
                s.add_to_clause(0, true);
                s.terminate_clause();
                s.add_to_clause(0, false);
                s.terminate_clause();
                return s;
            }
        }

        s.reserve(); // Reserve new variables

        // Reserve space
        std::vector<std::size_t> vars {};
        vars.reserve(4);

        /* Go through relation == Go through trees (index) with relation variables (var_arr)
         * Fast version of `for (index : trees) { var_arr = relation[index] … }`
         */
        for (auto const& [index, var_arr] : relation) {
            Tents::Coo const coo = t.index_to_coo(index);

            /* connect new relation variables to the previous (tents) variables by:
             * (relation with tent in direction dir) => (tent in direction dir) respectively
             * ¬(tent in direction dir) => ¬(relation with tent in direction dir) or as a clause
             * (tent in direction dir) OR ¬(relation with tent in direction dir)
             * for all directions u, r, d, l if there is a possible tent
             *
             * Also add variables to vars vector
             */
            for (Tents::Direction const dir : Tents::directions)
                if (var_arr[dir]) { // => possible tent in direction dir
                    s.add_to_clause(t.adj_coo(coo, dir)->index, true);
                    s.add_to_clause(var_arr[dir], false);
                    s.terminate_clause();
                    vars.push_back(var_arr[dir]);
                }


            // exactly one tent belongs to the tree (vars.size() >= 1)
            exactly_k(s, 1, vars);

            vars.clear();


            /* one tent belongs to at most one tree
             * we need to check rr, rd, dr, dd, dl, ld
             * the other directions have already been accounted for at previous `index`
             */

            // case up already accounted for

            if (var_arr[Tents::right]) { // => possible tent right
                Tents::Coo const r = *t.right_coo(coo);

                // ru already accounted for

                if (auto const rr = t.right_coo(r); rr && t.cell(*rr) == Tents::Tree) {
                    // mutual exclusion with rr, the tent right cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::right], false);
                    s.add_to_clause(relation[rr->index][Tents::left], false);
                    s.terminate_clause();
                }

                if (auto const rd = t.down_coo(r); rd && t.cell(*rd) == Tents::Tree) {
                    // mutual exclusion with rd, the tent right cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::right], false);
                    s.add_to_clause(relation[rd->index][Tents::up], false);
                    s.terminate_clause();
                }
            }

            if (var_arr[Tents::down]) { // => possible tent below
                Tents::Coo const d = *t.down_coo(coo);

                if (auto const dr = t.right_coo(d); dr && t.cell(*dr) == Tents::Tree) {
                    // mutual exclusion with dr, the tent down cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::down], false);
                    s.add_to_clause(relation[dr->index][Tents::left], false);
                    s.terminate_clause();
                }

                if (auto const dd = t.down_coo(d); dd && t.cell(*dd) == Tents::Tree) {
                    // mutual exclusion with dd, the tent down cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::down], false);
                    s.add_to_clause(relation[dd->index][Tents::up], false);
                    s.terminate_clause();
                }

                if (auto const dl = t.left_coo(d); dl && t.cell(*dl) == Tents::Tree) {
                    // mutual exclusion with dl, the tent down cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::down], false);
                    s.add_to_clause(relation[dl->index][Tents::right], false);
                    s.terminate_clause();
                }
            }

            if (var_arr[Tents::left]) { // => possible tent left
                // lu and ll already accounted for

                if (auto const ld = t.down_coo(*t.left_coo(coo)); ld && t.cell(*ld) == Tents::Tree) {
                    // mutual exclusion with ld, the tent left cannot belong to both trees
                    s.add_to_clause(var_arr[Tents::left], false);
                    s.add_to_clause(relation[ld->index][Tents::up], false);
                    s.terminate_clause();
                }
            }
        }
    }


    { // CNF: tents in rows/columns must correspond to the given numbers
        // Reserve space
        std::vector<std::size_t> elements {};
        elements.reserve(std::max(n, m));

        // tents in rows must correspond to the given numbers
        verbose_output("Adding rule to CNF: tents in rows must correspond to the given numbers ...");
        s.add_comment("tents in rows must correspond to the given numbers");

        for (std::size_t row = 0; row < n; ++row) {
            std::size_t const k = t.number_of_tents_in_row(row);

            // case k == 0 already covered (remove classification for cells in rows labelled with 0)
            if (!k)
                continue;
            
            for (std::size_t col = 0; col < m; ++col)
                if (std::size_t const index = t.rc_to_index(row, col); t_poss[index])
                    elements.push_back(index);

            if (elements.size() < k) {
                // UNSAT, number of possible tent locations is less than the number of tents
                verbose_output("Number of possible tent locations is less than the number of tents in row!");
                s.add_comment("Number of possible tent locations is less than the number of tents in row");
                s.add_to_clause(0, true);
                s.terminate_clause();
                s.add_to_clause(0, false);
                s.terminate_clause();
                return s;
            }

            // We need exactly k variables to be true
            exactly_k(s, k, elements);

            elements.clear();
        }

        // tents in columns must correspond to the given numbers
        verbose_output("Adding rule to CNF: tents in columns must correspond to the given numbers ...");
        s.add_comment("tents in columns must correspond to the given numbers");

        for (std::size_t col = 0; col < m; ++col) {
            std::size_t const k = t.number_of_tents_in_column(col);

            // case k == 0 already covered (remove classification for cells in columns labelled with 0)
            if (!k)
                continue;

            for (std::size_t row = 0; row < n; ++row)
                if (std::size_t const index = t.rc_to_index(row, col); t_poss[index])
                    elements.push_back(index);

            if (elements.size() < k) {
                // UNSAT, the number of possible tent locations is less than the number of tents
                verbose_output("Number of possible tent locations is less than the number of tents in column!");
                s.add_comment("Number of possible tent locations is less than the number of tents in column");
                s.add_to_clause(0, true);
                s.terminate_clause();
                s.add_to_clause(0, false);
                s.terminate_clause();
                return s;
            }

            // We need exactly k variables to be true
            exactly_k(s, k, elements);

            elements.clear();
        }
    }

    return s;
}
