/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tents.hpp" // Tents
#include "solver.hpp" // Solver
#include "cnf.hpp" // exactly_k
#include "arguments.hpp" // verbose_output
#include <map>
#include <algorithm> // std::equal

// Test if there is a tent around pos
bool surrounded_by_tents(Tents const& t, Tents::Coo const pos) {
    // Clockwise: ul -> u -> ur -> r -> dr -> d -> dl -> l

    if (auto const u = t.up_coo(pos); u) {
	if (auto const ul = t.left_coo(*u); ul && t.cell(*ul) == Tents::Tent) // tent in ul
	    return true;

	if (t.cell(*u) == Tents::Tent) // tent in u
	    return true;

	if (auto const ur = t.right_coo(*u); ur && t.cell(*ur) == Tents::Tent) // tent in ur
	    return true;
    }

    if (auto const r = t.right_coo(pos); r && t.cell(*r) == Tents::Tent) // tent in r
	return true;

    if (auto const d = t.down_coo(pos); d) {
	if (auto const dr = t.right_coo(*d); dr && t.cell(*dr) == Tents::Tent) // tent in dr
	    return true;

	if (t.cell(*d) == Tents::Tent) // tent in d
	    return true;

	if (auto const dl = t.left_coo(*d); dl && t.cell(*dl) == Tents::Tent) // tent in dl
	    return true;
    }

    if (auto const l = t.left_coo(pos); l && t.cell(*l) == Tents::Tent) // tent in l
	return true;

    return false;
}


// Validate Tents solution (checks if t is a valid Tents solution)
bool validate_tents(Tents const& t) {
    // check if t is a valid Tents puzzle (n,m > 0, number of trees = number of tents)
    if (!t.is_valid_puzzle())
        return false;

    // information from t

    std::size_t const n = t.n(), m = t.m();

    std::vector<std::size_t> const& trees {t.indices_of_trees()};
    std::vector<std::size_t> const& tents {t.indices_of_tents()};

    { // check whether the numbers of tents match
        verbose_output("Checking wheter the number of tents match ...");

        std::vector<std::size_t> row_counts (n, 0);
        std::vector<std::size_t> col_counts (m, 0);

        for (std::size_t const tent : tents) {
            Tents::Coo const coo = t.index_to_coo(tent);
            ++row_counts[coo.row];
            ++col_counts[coo.column];
        }

        if (!std::equal(row_counts.begin(), row_counts.end(), t.number_of_tents_in_rows()))
          return false;

        if (!std::equal(col_counts.begin(), col_counts.end(), t.number_of_tents_in_columns()))
          return false;
    }


    // check that no tents are directly adjacent to each other 
    verbose_output("Checking that no tents are directly adjacent to each other ...");

    for (std::size_t const index : tents)
        if (surrounded_by_tents(t, t.index_to_coo(index)))
            return false;


    // check 1:1 correspondence between trees and tents
    verbose_output("Checking 1:1 correspondence between trees and tents ...");

    Solver s {4 * trees.size()};

    // map index of Tree in `t` to index in `trees`
    std::map<std::size_t, std::size_t> index_map {};

    // fill index_map
    for (std::size_t tree_index = 0; tree_index < trees.size(); ++tree_index)
        index_map[trees[tree_index]] = tree_index;

    for (std::size_t tree_index = 0; tree_index < trees.size(); ++tree_index) {
        Tents::Coo const coo = t.index_to_coo(trees[tree_index]);

        std::vector<std::size_t> tr_poss {}; // possible tent-tree relations from the current tree
        tr_poss.reserve(4);

        for (Tents::Direction const dir : Tents::directions)
            if (auto const dir_coo = t.adj_coo(coo, dir); dir_coo && t.cell(*dir_coo) == Tents::Tent)
                tr_poss.push_back(4 * tree_index + dir);

        if (tr_poss.size() == 0) // no tent adjacent to tree
            return false;

        exactly_k(s, 1, tr_poss);

        for (std::size_t const rel : tr_poss) {
            std::vector<std::size_t> te_poss; // other relations for the current tent
            te_poss.reserve(4);

            Tents::Direction const north = static_cast<Tents::Direction>(rel % 4);
            Tents::Coo const n_coo = *t.adj_coo(coo, north);

            // Go through relative directions north, east, west
            for (Tents::Direction const dir : {north, Tents::rotate(north, 1), Tents::rotate(north, 3)})
                // Check if the cell in nd also is a tree
                if (auto const nd_coo = t.adj_coo(n_coo, dir); nd_coo && t.cell(*nd_coo) == Tents::Tree)
                    // Get index of tree and add variable to te_poss
                    te_poss.push_back(4 * index_map[nd_coo->index] + Tents::rotate(dir, 2));

            if (!te_poss.empty()) {
                te_poss.push_back(rel);
                exactly_k(s, 1, te_poss);
            }
        }
    }

    // For every tree there is exactly one corresponding tent

    return s.solve(); // if the problem is satisfiable
}
