/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "arguments.hpp" // Argument parsing
#include "tents.hpp" // Tents
#include "cnf.hpp" // gen_cnf
#include "solver.hpp" // Solver
#include "timer.hpp"
#include <iostream> // std::cin, std::cerr, std::cout
#include <fstream> // std::ifstream, std::ofstream
#include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE
#include <cerrno> // errno
#include <cstring> // std:strerror

namespace {
// Returns false if an error occured and true otherwise
bool input_output_tents() {
    Tents t {};

    if (!arguments.input_file) { // Read from stdin
        if (!(std::cin >> t))
            return false; // Error reading t (already printed out error message)
    } else { // Read from input_file
        std::ifstream is {arguments.input_file};

        if (!is) {
            std::cerr << "Failed to open file " << arguments.input_file
                      << ": " << std::strerror(errno) << '\n';
            return false;
        }

        if (!(is >> t))
            return false; // Error reading t (already printed out error message)
    }

    if (!t.is_valid_puzzle()) { // t is invalid
        std::cerr << "Invalid Tents-puzzle.\n";
        return false;
    }

    if (arguments.verbose)
        std::clog << "Input puzzle:\n" << t << std::flush;

    if (!arguments.read_solver_output || arguments.cnf_output || arguments.time) {
        // We want to use CaDiCaL and solve the CNF
        bool const solve = (arguments.output && !arguments.read_solver_output) || arguments.time;

        // Generate CNF
        verbose_output("Generating the CNF ...");

        if (arguments.time)
            clock_gen_start();

        Solver s = gen_cnf(t, arguments.cnf_output, solve);

        if(arguments.time)
            clock_gen_end();

        if (arguments.cnf_output) { // Write DIMACS
            verbose_output("Writing DIMACS ...");

            if (!arguments.cnf_output_file) { // Write to stdout
                s.write_dimacs(std::cout);
            } else { // Write to cnf_output_file
                std::ofstream os {arguments.cnf_output_file};

                if (!os) {
                    std::cerr << "Failed to open file " << arguments.cnf_output_file
                              << ": " << std::strerror(errno) << '\n';
                    return false;
                }

                s.write_dimacs(os);
            }

            verbose_output("Wrote DIMACS!");
        }

        if (solve) { // Solve
            verbose_output("Solving ...");

            if(arguments.time)
                clock_solver_start();

            if (!s.solve()) {
                std::cerr << "Tents puzzle is unsolvable!\n";
                return false;
            }

            if (arguments.time)
                clock_solver_end();

            verbose_output("Solved!");

            if (arguments.time)
                print_time_csv(s.get_num_clauses(), s.get_num_var(), arguments.input_file);

            add_tents_from_solver(t, s);
        }
    }

    if (arguments.read_solver_output) {
        verbose_output("Reading SAT solver output ...");

        if (!arguments.read_solver_output_file) { // Read from stdin
            if (!add_tents_from_solver_output(t, std::cin))
                return false; // Error (already printed out error message)
        } else { // Read from read_solver_output_file
            std::ifstream is {arguments.read_solver_output_file};

            if (!is) {
                std::cerr << "Failed to open file " << arguments.read_solver_output_file
                          << ": " << std::strerror(errno) << '\n';
                return false;
            }

            if (!add_tents_from_solver_output(t, is))
                return false; // Error (already printed out error message)
        }
    }

    if (arguments.output) { // Output solution
        if (!arguments.output_file) { // Write to stdout
            std::cout << t << std::flush;
        } else { // Write to file
            std::ofstream os {arguments.output_file};

            if (!os) {
                std::cerr << "Failed to open file " << arguments.output_file
                          << ": " << std::strerror(errno) << '\n';
                return false;
            }

            os << t;
        }
    }

    return true;
}

bool validate_solution() {
    Tents t {};

    if (!arguments.validate_file) { // Read from stdin
        if (!(std::cin >> t))
            return false; // Error reading t (already printed out error message)
    } else { // Read from validate_file
        std::ifstream is {arguments.validate_file};

        if (!is) {
            std::cerr << "Failed to open file " << arguments.validate_file
                      << ": " << std::strerror(errno) << '\n';
            return false;
        }

        if (!(is >> t))
            return false; // Error reading t (already printed out error message)
    }

    verbose_output("Validating ...");

    if (validate_tents(t)) {
        std::cout << "Tents solution is valid." << std::endl;
        return true;
    } else {
        std::cout << "Tents solution is invalid." << std::endl;
        return false;
    }
}

// Returns false if an error occured and true otherwise
bool generate_tents() {
    // gen_tents writes verbose_output
    Tents const t = arguments.use_seed ? gen_tents(arguments.size_n, arguments.size_m,
                                            arguments.difficulty, arguments.seed)
                            : gen_tents(arguments.size_n, arguments.size_m, arguments.difficulty);

    if (!arguments.generate_file) { // Write to stdout
        std::cout << t << std::flush;
    } else { // Write to generate_file
        std::ofstream os {arguments.generate_file};

        if (!os) {
            std::cerr << "Failed to open file " << arguments.generate_file
                      << ": " << std::strerror(errno) << '\n';
            return false;
        }

        os << t;
    }

    return true;
}
}

int main(int argc, char* argv[]) {
    switch (parse_arguments(argc, argv)) { // parse command line arguments
    case -1:
        return EXIT_FAILURE;
    case 1:
        return EXIT_SUCCESS;
    }

    // Input and output Tents
    if (arguments.input && !input_output_tents())
        return EXIT_FAILURE;

    // Validate Tents solution
    if (arguments.validate && !validate_solution())
        return EXIT_FAILURE;

    // Generate Tents
    if (arguments.generate && !generate_tents())
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}
