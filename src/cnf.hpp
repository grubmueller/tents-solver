/* Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CNF_HPP
#define CNF_HPP

#include <vector>
#include <iosfwd> // std::istream
#include <cstddef> // std::size_t

class Tents;
class Solver;

// generate_cnf.cpp

/* Function gen_cnf:
 *
 * input: an instance of Tents (which must be valid, see precondition)
 *
 * output: an instance of Solver containing the CNF corresponding to the input
 *
 * precondition: t.is_valid_puzzle()
 */
Solver gen_cnf(Tents const& t, bool save_cnf = false, bool use_cadical = true);

// exactly_k.cpp

/* Add clauses to s, such that exactly k of elements must be true
 *
 * precondition: k <= elements.size()
 */
void exactly_k(Solver& s, std::size_t k, std::vector<std::size_t> const& elements);

// solution.cpp

// Add tents to t from s, s must be solved and satisfiable
void add_tents_from_solver(Tents& t, Solver const& s);

/* Add tents to t from a solver output
 *
 * format:
 * lines beginning with "c " are comments and ignored
 * empty lines are ignored
 * lines "s SATISFIABLE", "SAT" and "SATISFIABLE" mean SAT
 * lines beginning with "s " and not "s SATISFIABLE" mean UNSAT
 * lines "UNSAT" and "UNSATISFIABLE" mean UNSAT
 * lines beginning with "v " or lines starting with a number or - contain the values of a solution
 *
 * return value:
 * true if the output is satisfiable and the resulting Tents solution is valid
 * false otherwise
 */
bool add_tents_from_solver_output(Tents& t, std::istream& is);

#endif // CNF_HPP
