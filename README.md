# Tents-Solver

## Introduction

This piece of software contains a Solver for the puzzle *Tents*. It was created as part of the lecture *SAT Solving* of the winter term 2020/2021 at LMU Munich.

To solve puzzles it first translates them to a formula in CNF, then passes it to a SAT solver
([`CaDiCaL`](http://fmv.jku.at/cadical/)) and finally outputs the solution to a specified path.

Other features:

* output the formula as DIMACS CNF
* generate new puzzles
* validate a solution

The software runs on Linux and MacOS (in fact it should run on any POSIX compatible system).
Windows is currently not supported.

The `GitLab` repository is located at [https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver](https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver).

For validation purposes we generated lots of random Tents puzzles. If your copy of this project does
not include them, see the `GitLab` repository (they are located in the subfolder `testing`) or visit
our puzzle repository at [https://syncandshare.lrz.de/getlink/fi6NBvYpmzpBP14GH2MAFXgj/](https://syncandshare.lrz.de/getlink/fi6NBvYpmzpBP14GH2MAFXgj/).

## Building Tents-Solver

### 0. Preparation

Make sure you have a `C++` compiler installed, e. g. `g++` or `clang++`. The minimum required standard is `C++17`. To build the project it is essential to have `cmake` version `3.10` or newer properly configured on your system.

The GUI version uses the `tcl/tk` GUI toolkit. It comes preinstalled with MacOS and most linux distributions.

The GUI is written in `Python 3` using [`Tkinter`](https://tkdocs.com/tutorial/install.html#installlinux) for the graphical core functionality and [`Pillow`](https://pillow.readthedocs.io/en/stable/installation.html) for image handling.

Before using the GUI version make sure that `tcl/tk` and `Python 3` are installed on your system. The python packages `Tkinter` and `Pillow` are included in most Python installations.

### 1. Download

If you don't already have the project files, you can download them by using `git clone` via `https` or `ssh`:

```sh
$ git clone git@gitlab2.cip.ifi.lmu.de:grubmueller/tents-solver.git
```

or

```sh
$ git clone https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver.git
```

You can also download them directly from the [`GitLab` website](https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver) by clicking on the *download* button.

### 2. Configure build system

Unless already done, start your shell and make sure that the current directory is `tents-solver`. To see the current directory use

```sh
$ pwd
```

To change the current director type

```sh
$ cd <path>
```

where `<path>` should be substituted by the desired path.

To configure the build system execute

```sh
$ ./configure
```

You will be informed of any additional steps you have to make, e. g. installing `CaDiCaL` (see below).

### 3. Install CaDiCaL

Tents-Solver requires the SAT solver `CaDiCaL`. You can skip this section if `CaDiCaL` is already installed system-wide or `./configure` found a valid `CaDiCaL` installation otherwise. Please note that there is a bug in `CaDiCaL` versions prior to `1.3` where the input of a (very) large Tents puzzle causes `CaDiCaL` to crash. This has been fixed in version `1.3`. We recommend using the latest version of `CaDiCaL`.

There are generally two options to choose from when using `CaDiCaL` with Tents-Solver:

* use an already compiled version of `CaDiCaL`
* compile `CaDiCaL` yourself

#### Use an already compiled version of CaDiCaL

If you already have a compiled version of `CaDiCaL` you can use it by placing the files `libcadical.a` and `cadical.hpp` in the folder `build/cadical` and execute

```sh
$ ./configure
```

#### Compile CaDiCaL yourself

Make sure, your current directory is `tents-solver`. Then execute

```sh
$ make cadical
```

The latest version of `CaDiCaL` will be downloaded and compiled automatically. The generated files will be placed in the folder

```sh
build/cadical
```

### 4. Compile Tents-Solver

With `tents-solver` as current directory, simply execute

```sh
$ make release
```

Tents-Solver will be compiled to the executable `build/release/tents-solver`.

## Using Tents-Solver

### CLI version

To use Tents-Solver's CLI version simply use the following command:

```sh
$ /path/to/folder/tents-solver [options]
```

where `/path/to/folder` is the path to the folder where the executable `tents-solver` is located. For a detailed list of options see the following table:

Option      | Description
----------- | -----------
`-h`        | Display the help text.
`-l`        | Display the license of this program including the authors.
`-v`        | Use a more verbose output.
`-p <path>` | Specify the path from where to load the puzzle.
`-o <path>` | Write the solution to the file at the specified path.
`-u`        | Use the unoptimised version: deactivate the major performance enhancements.
`-z`        | Measure the time used for CNF generation the solving. Output a csv line.
`-c <path>` | Write the CNF as DIMACS CNF to the file at the specified path.
`-r <path>` | Read the output of a SAT solver to decode and validate the solution.
`-t <path>` | Test/validate the solution at the specified path.
`-g <path>` | Generate a new puzzle. The newly created puzzle is written to the specified path.
`-n <N>`    | Specify the number of rows (default 10) for the generated puzzle.
`-m <M>`    | Specify the number of columns (default 10) for the generated puzzle.
`-d <D>`    | Use difficulty D (default 5) for the generated puzzle. D has to be an integer number from `1` to `10`.
`-s <seed>` | Use the following seed (unsigned 32-bit integer) to generate the puzzle.

To read from standard input or write to standard ouput substitute `<path>` by `'-'` (the dash character at ASCII `x2D`).

#### Examples

In the following it is assumed that the specified input file `tents.txt` containing the Tents puzzle does actually exist.

Solve the puzzle `tents.txt` and write the solution to the file `tents.solv.txt`:

```sh
$ /path/to/folder/tents-solver -p tents.txt -o tents.solv.txt
```

Solve the puzzle `tents.txt` and write the solution to the standard output:

```sh
$ /path/to/folder/tents-solver -p tents.txt -o -
```

Read the puzzle from standard input and write the solution to the file `tents.solv.txt`:

```sh
$ /path/to/folder/tents-solver -p - -o tents.solv.txt
```

Solve the puzzle `tents.txt` and write the solution to the file `tents.solv.txt`. Additionally write the DIMACS CNF to the file `tents.cnf`:

```sh
$ /path/to/folder/tents-solver -p tents.txt -o tents.solv.txt -c tents.cnf
```

Write the DIMACS CNF for the puzzle `tents.txt` to the file `tents.cnf`, use the external SAT solver
`cadical` to solve, read its output and output the solution to standard output:

```sh
$ /path/to/folder/tents-solver -p tents.txt -c tents.cnf
$ cadical tents.cnf | /path/to/folder/tents-solver -p tents.txt -o - -r -
```

Generate a 10x15 puzzle with difficulty 7 and write it to `tents.gen.txt` (notice, that `-n 10` can be omitted since this is the default value):

```sh
$ /path/to/folder/tents-solver -g tents.gen.txt -m 15 -d 7
```

Generate a 20x15 puzzle with difficulty 5, solve it and write the solution to the file
`tents.solv.txt` as well as the DIMACS CNF to the file `tents.cnf`
(notice, that `-d 5` can be omitted since this is the default value):

```sh
$ /path/to/folder/tents-solver -g - -n 20 -m 15 |
            /path/to/folder/tents-solver -p - -o tents.solv.txt -c tents.cnf
```

### GUI version

#### Running the GUI

Make sure you already compiled the CLI version and your working directory is `tents-solver`. To run the GUI execute

```sh
$ ./tents-solver-gui.py
```

or

```sh
$ python3 tents-solver-gui.py
```

#### Functionality

The GUI version is meant to provide a more visually appealing approach than the CLI version. Likewise it is possible to solve and generate new puzzles. In addition, it provides an intuitive interface to create Tents puzzles on your own. At any point, clicking the button `back` brings you back to the initial screen.

##### Create your own puzzle

Click `Create Field` and specify positive integer values for `n` and `m`. Edit the field by clicking on the individual fields, cycling through `tent`, `tree`, `grass`. Empty entries for the tent-numbers in rows and columns will be treated as zero.

To save the puzzle as a file, click the button `Save Puzzle`.

##### Import an existing puzzle

Click the button labeled `Load Puzzle from file` to import a file into the puzzle creator.

##### Solve a puzzle

It is possible to have a puzzle solved directly by clicking the button labeled `Solve Puzzle from file`.

##### Generate a puzzle

Additionally you can generate a random puzzle by clicking the Button labeled `Generate Puzzle` and afterwards specifying the dimensions in the prompt.


## Performance of Tents-Solver

Tents-Solver has major performance enhancements. If you don't want to use them, use the option `-u` (see above).

For details about the performance of Tents-Solver, see the evaluation at `testing/evaluate.pdf`.

If your copy of this project does not include the testing data in the folder `testing`, see the [`GitLab` repository](https://gitlab2.cip.ifi.lmu.de/grubmueller/tents-solver).
