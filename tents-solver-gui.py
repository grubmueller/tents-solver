#!/usr/bin/env python3

# Copyright (C) 2020 Lukas Matthias Bartl, Christian Benedikt Dietze, Fabian Lukas Grubmüller, Luca Alessio Maio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PIL import ImageTk
from tkinter import *
from tkinter import filedialog as fd
from functools import partial
import subprocess

# Function that builds the grid for the custom puzzles
def start_with_dimensions(n_entry_old, m_entry_old):
    if n_entry_old == '' or m_entry_old == '':  # check if there was an input
        new_window = Toplevel()
        Label(new_window, text = "Please input dimensions below!", padx = 15, pady = 15) .grid(row = 0, column = 0)
        return
    if not n_entry_old.isdigit() or not m_entry_old.isdigit():
        new_window = Toplevel()
        Label(new_window, text = "Please input numbers as dimensions!", padx = 15, pady = 15) .grid(row = 0, column = 0)
        return
    global entry_array_n
    global entry_array_m
    n = int(n_entry_old)
    m = int(m_entry_old)
    clear_window()
    Button(window, text = 'Back', width = 20, command = back_to_main_window) .grid(row = 0, column = 0, sticky = N)
    Button(window, text = 'Recreate Field', width = 20, command = partial(dimensions_prompt, 0)) .grid(row = 1, column = 0, sticky = N)
    Button(window, text = 'Write Puzzle to file', width = 20, command = encode_to_file) .grid(row = 2, column = 0, sticky = N)
    Button(window, text = 'Solve Puzzle', width = 20, command = solve_from_editor) .grid(row = 3, column = 0, sticky = N)
    Button(window, text = 'Load Puzzle from file', width = 20, command = import_to_puzzle_editor) .grid(row = 4, column = 0, sticky = N)
    Label(window, text = 'Dimensions: ' + str(n) + 'x' + str(m)).grid(row = 5, column = 0, sticky = N)
    global button_array_pictures    # used to store which picture is at the moment displayed on a field (button)
    button_array_pictures = [[0 for x in range(m)] for x in range(n)]
    button_array.clear()
    entry_array_n.clear()
    entry_array_m.clear()
    # create the actual grid of fields (buttons) and entries
    for x in range(n):
        button_array.append(list())
        for y in range(m):
            button_array[x].append(Button(window, image = grass, command = partial(change_picture, x, y), border = 0, padx = 0, pady = 0)) 
            button_array[x][y].grid(row = x, column = y + 1)
        entry_array_n.append(Entry(window, width = 4)) 
        entry_array_n[x].grid(row = x, column = m + 2)
    for i in range(m):
        entry_array_m.append(Entry(window, width = 4))
        entry_array_m[i].grid(row = n + 1, column = i + 1)

# function that gets called when a field is clicked and changes the picture to the next
def change_picture(x, y):
    if button_array_pictures[x][y] == 0:
        button_array[x][y].config(image =  tree)
        button_array_pictures[x][y] = 1
    else: 
        if button_array_pictures[x][y] == 1:
            button_array[x][y].config(image = tent)
            button_array_pictures[x][y] = 2
        else: 
            if button_array_pictures[x][y] == 2:
                button_array[x][y].config(image = grass)
                button_array_pictures[x][y] = 0

# function that gets called when the "Write Puzzle to File" button is pressed
def encode_to_file():
    n = len(entry_array_n)
    m = len(entry_array_m)
    encoding = ''
    encoding += str(n) + ' ' + str(m) + '\n'
    for x in range(n):
        for y in range(m):
            if button_array_pictures[x][y] == 0:
                encoding += '.'
            if button_array_pictures[x][y] == 1:
                encoding += 'T'
            if button_array_pictures[x][y] == 2:
                encoding += 'X'
        if entry_array_n[x].get() == '' or not entry_array_n[x].get().isdigit():
            encoding += ' 0\n'
        else:
            encoding += ' ' + entry_array_n[x].get() + '\n'
    for i in range(m):
        if entry_array_m[i].get() == '' or not entry_array_m[i].get().isdigit():
            encoding += '0 '
        else:
            encoding += entry_array_m[i].get() + ' '
            
    encoding += '\n'
    file = fd.asksaveasfilename()
    with open(file, 'w') as fw:
        fw.write(encoding)

# function that gets called when the "Solve Puzzle" button on the custom puzzle screen is pressed
def solve_from_editor():
    n = len(entry_array_n)
    m = len(entry_array_m)
    encoding = ''
    encoding += str(n) + ' ' + str(m) + '\n'
    for x in range(n):
        for y in range(m):
            if button_array_pictures[x][y] == 0:
                encoding += '.'
            if button_array_pictures[x][y] == 1:
                encoding += 'T'
            if button_array_pictures[x][y] == 2:
                encoding += 'X'
        if entry_array_n[x].get() == '' or not entry_array_n[x].get().isdigit():
            encoding += ' 0\n'
        else:
            encoding += ' ' + entry_array_n[x].get() + '\n'
    for i in range(m):
        if entry_array_m[i].get() == '' or not entry_array_m[i].get().isdigit():
            encoding += '0 '
        else:
            encoding += entry_array_m[i].get() + ' '
            
    encoding += '\n'
    output = subprocess.run(['./build/release/tents-solver', "-o", "-", "-p", "-"], capture_output=True, input = encoding.encode()) # runs the solver and captures output
    decode_from_str(output)

# function that is called when the "Solve Puzzle" button on the first screen is pressed
def solve_puzzle():
    file = fd.askopenfilename()
    output = subprocess.run(['./build/release/tents-solver', "-o", "-", "-p", file], capture_output=True) # runs the solver and captures output
    decode_from_str(output)

# function that is called when "Generate Puzzle" button on the first screen is pressed
def generate_puzzle_to_file(n, m, d):
    file = fd.asksaveasfilename()
    if d == '':
        d = '5'
    if n == '':
        n = '10'
    if m == '':
        m = '10'
    output = subprocess.run(['./build/release/tents-solver', "-g", file, "-n", n, "-m", m, "-d", d], capture_output=True)
    if output.returncode != 0:
        new_window = Toplevel()
        Label(new_window, text = output.stderr.decode(), padx = 15, pady = 15) .grid(row = 0, column = 0)
        return

# function that decodes the given puzzle into a graphical format
def decode_from_str(solver_output):
    puzzle = solver_output.stdout.decode()
    global picture_array
    global tents_number_x
    global tents_number_y
    if solver_output.returncode != 0:
        new_window = Toplevel()
        Label(new_window, text = solver_output.stderr.decode(), padx = 15, pady = 15) .grid(row = 0, column = 0)
        return
    lines = puzzle.splitlines()
    secondPart = False
    dimensions = lines[0]
    n_str = ''
    m_str = ''
    for char in dimensions:
        if char == ' ':
            secondPart = True
            continue
        if secondPart != True:
            n_str += char
        else:
            m_str += char
    n = int(n_str)
    m = int(m_str)
    picture_array = [[0 for x in range(m)] for x in range(n)] 
    tents_number_x = ['' for x in range(n)]
    tents_number_y = ['' for x in range(m)]
    x = 0
    y = 0
    for line in lines[1:]:
        if line.strip():
            if x < n:
                for char in line:
                    if y < m:
                        if char == '.':
                            picture_array[x][y] = 0
                        if char == 'T':
                            picture_array[x][y] = 1
                        if char == 'X':
                            picture_array[x][y] = 2
                        y += 1
                        continue
                    tents_number_x[x] += char
                y = 0
                x += 1
                continue
            else:
                for char in line:
                    if char == ' ':
                        y += 1
                        continue    
                    tents_number_y[y] += char
    clear_window()
    Button(window, text = 'Back', command = back_to_main_window) .grid(row = 0, column = 0, sticky = N)
    Button(window, text = 'Save', command = save_solution) .grid(row = 1, column = 0, sticky = N)
    for x in range(n):
        for y in range(m):
            if(picture_array[x][y] == 0):
                Label(window, image = grass, border = 0, padx = 0, pady = 0).grid(row = x, column = y + 1)
            if(picture_array[x][y] == 1):
                Label(window, image = tree, border = 0, padx = 0, pady = 0).grid(row = x, column = y + 1)
            if(picture_array[x][y] == 2):
                Label(window, image = tent, border = 0, padx = 0, pady = 0).grid(row = x, column = y + 1)
        Label(window, text = tents_number_x[x], border = 0, padx = 0, pady = 0).grid(row = x, column = m + 2)
    for i in range(m):
        Label(window, text = tents_number_y[i], border = 0, padx = 0, pady = 0).grid(row = n + 1, column = i + 1)

def save_solution():
    n = len(tents_number_x)
    m = len(tents_number_y)
    encoding = ''
    encoding += str(n) + ' ' + str(m) + '\n'
    for x in range(n):
        for y in range(m):
            if picture_array[x][y] == 0:
                encoding += '.'
            if picture_array[x][y] == 1:
                encoding += 'T'
            if picture_array[x][y] == 2:
                encoding += 'X'
        encoding += tents_number_x[x] + '\n'
    for i in range(m):
            encoding += tents_number_y[i] + ' '
            
    encoding += '\n'
    file = fd.asksaveasfilename()
    with open(file, 'w') as fw:
        fw.write(encoding)

# function that gets called when the "Load Puzzle from File" button is pressed
def import_to_puzzle_editor():
    file = fd.askopenfilename()
    global picture_array
    global tents_number_x
    global tents_number_y
    with open(file, 'r') as fr:
        secondPart = False
        dimensions = fr.readline()
        n_str = ""
        m_str = ""
        for char in dimensions:
            if char == " ":
                secondPart = True
                continue
            if secondPart != True:
                n_str += char
            else:
                m_str += char
        n = int(n_str)
        m = int(m_str)
        temp_n_entry = Entry()
        temp_n_entry.insert(0, n)
        temp_m_entry = Entry()
        temp_m_entry.insert(0, m)
        start_with_dimensions(temp_n_entry.get(), temp_m_entry.get())
        picture_array = [[0 for x in range(m)] for x in range(n)]
        tents_number_x = ['' for x in range(n)]
        tents_number_y = ['' for x in range(m)]
        x = 0
        y = 0
        for line in fr:
            if line.strip():
                if x < n:
                    for char in line:
                        if y < m:
                            if char == '.':
                                picture_array[x][y] = 0
                            if char == 'T':
                                picture_array[x][y] = 1
                            if char == 'X':
                                picture_array[x][y] = 2
                            y += 1
                            continue
                        tents_number_x[x] += char
                    y = 0
                    x += 1
                    continue
                else:
                    for char in line:
                        if char == ' ':
                            y += 1
                            continue 
                        else:   
                            if not char.isdigit():
                                continue
                        tents_number_y[y] += char
    for x in range(n):
        entry_array_n[x].delete(0, END)
        entry_array_n[x].insert(0, tents_number_x[x][1])
        for y in range(m):
            if(picture_array[x][y] == 0):
                button_array[x][y].config(image = grass)
                button_array_pictures[x][y] = 0
            if(picture_array[x][y] == 1):
                button_array[x][y].config(image = tree)
                button_array_pictures[x][y] = 1
            if(picture_array[x][y] == 2):
                button_array[x][y].config(image = tent)
                button_array_pictures[x][y] = 2
    for i in range(m):
        entry_array_m[i].delete(0, END)
        entry_array_m[i].insert(0, tents_number_y[i])

# function that is called when the "Back" button is pressed and once the Program is initially started
def back_to_main_window():
    clear_window()
    Button(window, text='Create Field', width = 25, command = partial(dimensions_prompt, 0)) .grid(row = 0, column = 0, sticky=N)
    Button(window, text='Solve Puzzle from file', width = 25, command = solve_puzzle) .grid(row = 1, column = 0, sticky = N)
    Button(window, text='Load Puzzle from file', width = 25, command = import_to_puzzle_editor) .grid(row = 2, column = 0, sticky = N)
    Button(window, text='Generate Puzzle', width = 25, command = partial(dimensions_prompt, 1)) .grid(row = 3, column = 0, sticky = N)

# function that creates a prompt for the dimensions of the generated filed or puzzle, depending of the value of win_type
def dimensions_prompt(win_type):
    popout = Toplevel()
    global n_entry
    global m_entry
    n_entry = Entry(popout, width = 6)
    n_entry.grid(row = 2, column = 0)
    Label(popout, width = 6, text = 'n').grid(row = 1, column = 0)
    m_entry = Entry(popout, width = 6)
    m_entry.grid(row = 2, column = 1)
    Label(popout, width = 6, text = 'm').grid(row = 1, column = 1)
    if win_type == 0:
        Label(popout, text = 'Please specify the dimensions of the Puzzle') .grid(row = 0, column = 0, columnspan = 2)
        Button(popout, text = 'submit', command = lambda:[start_with_dimensions(n_entry.get(), m_entry.get()), popout.destroy()]) .grid(row = 3, column = 0, columnspan = 2)
    if win_type == 1:
        Label(popout, text = 'Please specify the dimensions of the Puzzle') .grid(row = 0, column = 0, columnspan = 3)
        Label(popout, text = 'difficulty').grid(row = 1, column = 2)
        diff_entry = Entry(popout, width = 6)
        diff_entry.grid(row = 2, column = 2)
        Button(popout, text = 'submit', command = lambda:[generate_puzzle_to_file(n_entry.get(), m_entry.get(), diff_entry.get()), popout.destroy()]) .grid(row = 3, column = 0, columnspan = 3)


# function that clears all widgets out of the window
def clear_window():
    w_list = window.grid_slaves()
    for l in w_list:
        l.destroy()

window = Tk()
window.title('Tent Puzzle Inputter')

# the images displayed on the fields
global grass
global tent
global tree
grass = ImageTk.Image.open('res/background green.png')
grass = grass.resize((30, 30), ImageTk.Image.ANTIALIAS)
grass = ImageTk.PhotoImage(grass)
tent = ImageTk.Image.open('res/tent 2.png')
tent = tent.resize((30, 30), ImageTk.Image.ANTIALIAS)
tent = ImageTk.PhotoImage(tent)
tree = ImageTk.Image.open('res/tree realistic.png')
tree = tree.resize((30, 30), ImageTk.Image.ANTIALIAS)
tree = ImageTk.PhotoImage(tree)

button_array = list()
entry_array_n = list()
entry_array_m = list()

back_to_main_window()

window.mainloop()
